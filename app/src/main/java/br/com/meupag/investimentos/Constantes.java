package br.com.meupag.investimentos;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.MediaType;

public class Constantes {

    public static final String SUCCESSFUL_POST = "successful POST response: ";
    public static final String SUCCESSFUL_PUT = "successful PUT response: ";
    public static final String SUCCESSFUL_GET = "successful GET response: ";

    public static final String DB_INVISTA = "db-invista";
    public static final int PASSWORD_SIZE = 6;
    public static final int SIGNATURE_SIZE = 8;

    public static ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);
    }

    public static final String HOST_AWS                     = "https://app.avistafinanceira.com";

    public static final String WS_TRANSFER_DOCUMENT         = "/transferdocument";
    public static final String WS_PRODUCT_DOMAIN            = "/product/domain";
    public static final String WS_PRODUCT_DISTRICTS         = "/product/districts";
    public static final String WS_PRODUCT_CONTACT           = "/product/contact";
    public static final String WS_REKOGNITION_LABELS        = "/rekognition/labels";
    public static final String WS_ACCOUNT_ADDRESS           = "/account/address";
    public static final String WS_ACCOUNT_CREDENTIALS       = "/account/credentials";
    public static final String WS_ACCOUNT_PENDING           = "/account/pending";
    public static final String WS_ACCOUNT_PROPOSAL          = "/account/proposal";
    public static final String WS_ACCOUNT_LOGIN             = "/account/login";
    public static final String WS_ACCOUNT_RECOVER           = "/account/recover";
    public static final String WS_CUSTOMER_ACCOUNT          = "/customer/account";
    public static final String WS_CUSTOMER_PROFILE          = "/customer/profile";
    public static final String WS_CUSTOMER_BALANCE          = "/customer/balance";
    public static final String WS_CUSTOMER_EXTRACT          = "/customer/extract";
    public static final String WS_CUSTOMER_TOKEN            = "/customer/token";
    public static final String WS_CUSTOMER_WALLET_PREVIEW   = "/customer/walletpreview";
    public static final String WS_CUSTOMER_WITHDRAW         = "/customer/withdraw";
    public static final String WS_CUSTOMER_WALLET           = "/customer/wallet";
    public static final String WS_STOCK_RATES               = "/stock/rates";
    public static final String WS_STOCK_TAX                 = "/stock/tax";
    public static final String WS_STOCK_BUY                 = "/stock/buy";
    public static final String WS_STOCK_DELETE              = "/stock/delete";
    public static final String WS_STOCK_SELL                = "/stock/sell";
    public static final String WS_SENDEMAIL                 = "/sendmail";
    public static final String WS_SENDSMS                   = "/sendsms";


    public static final String TOKEN    = "Basic RjlFQ0RBRUY4QzEzRDoyODE5QzI2NTY0RjU2";
    public static final String AUTH_KEY = "Y0UB4CKV0LT3P4G4ND0M3N0S";


    //MOCKS
    public static final String CPF_FABIO = "104.240.567-04";

    public static final String FILE_PROVIDER = "br.com.meupag.investimentos.fileprovider";

}
