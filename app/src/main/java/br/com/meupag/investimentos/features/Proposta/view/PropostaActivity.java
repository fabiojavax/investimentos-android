package br.com.meupag.investimentos.features.proposta.view;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Switch;

import br.com.meupag.investimentos.BuildConfig;
import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.util.AWSCognitoUtil;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.CpfCnpjMasks;
import br.com.meupag.investimentos.util.FmtUtils;
import br.com.meupag.investimentos.util.IOUtils;
import br.com.meupag.investimentos.util.Masks;
import br.com.meupag.investimentos.util.RetornoListaWS;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.call.*;
import br.com.meupag.investimentos.ws.transferobject.AddressTO;
import br.com.meupag.investimentos.ws.transferobject.DistrictTO;
import br.com.meupag.investimentos.ws.transferobject.ProductDomainTO;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.util.StringUtils;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class PropostaActivity extends AppCompatActivity {

    private Activity activity = this;

    private ProductDomainTO productDomainTO;

    private ProgressDialog progressDialog;
    private EditText nomeCompletoEditText;
    private EditText emailEditText;
    private EditText estadoCivilEditText;
    private EditText cpfEditText;
    private EditText celularEditText;
    private EditText nomeMaeEditText;
    private EditText estadoNascimentoEditText;
    private EditText cidadeNascimentoEditText;
    private EditText profissaoEditText;
    private EditText cepEditText;
    private EditText bairroEditText;
    private EditText ruaEditText;
    private EditText cidadeEditText;
    private EditText complementoEditText;
    private EditText estadoEditText;
    private EditText numeroEditText;
    private Button   botaoBuscarCep;
    private EditText tipoDcumento;
    private EditText numeroDocEditText;
    private EditText estadoDocEditText;
    private EditText orgaoEmissorEditText;
    private EditText dataEmissaoEditText;
    private EditText dataVencimentoEditText;
    private EditText codBancoEditText;
    private EditText agenciaEditText;
    private EditText numContaEditText;
    private EditText nomeBancoEditText;
    private EditText tipoContaEditText;
    private Switch   termosSwitch;
    private Switch   ppeSwitch;
    private Button   btnEnviarProposta;



    private EditText dataNascimentoEditText;

    private String civilStateSelected;
    private String birthdayEstateSelected;
    private String birthdayCitySelected;
    private String occupationSelected;
    private String documentTypeSelected;
    private String estadoDocumentSelected;
    private String orgaoEmissorSelected;
    private String codeBankSelected;
    private String accountTypeSelected;
    private String gender = "M";

    private List<DistrictTO> districts = new ArrayList<>();

    private String [] civilStates;
    private String [] estados;
    private String [] cidades;
    private String [] profissoes;
    private String [] tiposDocumento;
    private String [] documetIssuers;
    private String [] banks;
    private String [] accountTypes;

    //S3 OBJECTS
    private TransferUtility transferUtility;
    private String photoPath;
    private File sourceFile;
    String nomeEnviado;
    private static int PICK_IMAGE_CAMERA = 1;
    private static int PICK_IMAGE_GALLERY = 2;

    private Integer selectedUploadAction = 0;
    private Integer UPLOAD_DOCUMENTO_FRENTE = 1;
    private Integer UPLOAD_DOCUMENTO_VERSO = 2;
    private Integer UPLOAD_DOCUMENTO_ENDERECO = 3;
    private Integer UPLOAD_DOCUMENTO_ASSINATURA = 4;
    private boolean hasDocumentoFrente;
    private boolean hasDocumentoVerso;
    private boolean hasDocumentoEndereco;
    private boolean hasDocumentoAssinatura;

    private View viewDocumentoFrente;
    private View viewDocumentoVerso;
    private View viewDocumentoEndereco;
    private View viewDocumentoAssinatura;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_proposta);

        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

        loadDomain();

        nomeCompletoEditText        = findViewById(R.id.et_nome_completo);
        emailEditText               = findViewById(R.id.et_email);
        cpfEditText                 = findViewById(R.id.et_cpf);
        celularEditText             = findViewById(R.id.et_celular);
        nomeMaeEditText             = findViewById(R.id.et_nome_mae);
        estadoCivilEditText         = findViewById(R.id.et_estado_civil);

        //dados complementares
        dataNascimentoEditText      = findViewById(R.id.et_data_nascimento);
        cidadeNascimentoEditText    = findViewById(R.id.et_cidade_nascimento);
        estadoNascimentoEditText    = findViewById(R.id.et_estado_nascimento);
        profissaoEditText           = findViewById(R.id.et_profissao);

        //endereço
        cepEditText                 = findViewById(R.id.et_cep);
        bairroEditText              = findViewById(R.id.et_bairro);
        ruaEditText                 = findViewById(R.id.et_nome_rua);
        numeroEditText              = findViewById(R.id.et_numero);
        cidadeEditText              = findViewById(R.id.et_cidade);
        estadoEditText              = findViewById(R.id.et_estado);
        complementoEditText         = findViewById(R.id.et_complemento);
        botaoBuscarCep              = findViewById(R.id.botao_buscar_cep);

        //documentos
        tipoDcumento                = findViewById(R.id.et_tipo_de_documento);
        numeroDocEditText           = findViewById(R.id.et_numero_documento);
        orgaoEmissorEditText        = findViewById(R.id.et_orgao_emissor);
        estadoDocEditText           = findViewById(R.id.et_estado_doc);
        dataEmissaoEditText         = findViewById(R.id.et_data_emissao);
        dataVencimentoEditText      = findViewById(R.id.et_data_vencimento);

        //dados bancários
        codBancoEditText            = findViewById(R.id.et_codigo);
        nomeBancoEditText           = findViewById(R.id.et_nome_banco);
        agenciaEditText             = findViewById(R.id.et_agencia);
        tipoContaEditText           = findViewById(R.id.et_tipo_conta);
        numContaEditText            = findViewById(R.id.et_numero_conta);

        //ppe
        ppeSwitch                   = findViewById(R.id.switch_ppe);

        //termos
        termosSwitch                = findViewById(R.id.switch_termos);

        btnEnviarProposta           = findViewById(R.id.btn_enviar_proposta);

        //set events
        estadoCivilEditText.setOnFocusChangeListener(onFocusEstadoCivil);
        estadoNascimentoEditText.setOnFocusChangeListener(onFocusEstadoNascimento);
        cidadeNascimentoEditText.setOnFocusChangeListener(onFocusCidadeNascimento);
        profissaoEditText.setOnFocusChangeListener(onFocusProfissao);
        cpfEditText.addTextChangedListener(CpfCnpjMasks.insert(cpfEditText));
        botaoBuscarCep.setOnClickListener(onClickBuscarCep);
        tipoDcumento.setOnFocusChangeListener(onFocusTipoDocumento);
        estadoDocEditText.setOnFocusChangeListener(onFocusEstadoDoc);
        orgaoEmissorEditText.setOnFocusChangeListener(onFocusOrgaoEmissor);
        codBancoEditText.setEnabled(false);
        nomeBancoEditText.setOnFocusChangeListener(onFocusNomeBanco);
        tipoContaEditText.setOnFocusChangeListener(onFocusTipoConta);
        Masks.setDataMask(dataEmissaoEditText);
        Masks.setDataMask(dataNascimentoEditText);
        Masks.setDataMask(dataVencimentoEditText);

        cpfEditText.setText(UserPrefs.getProfile(activity).customerId);

        if(BuildConfig.DEBUG){
            mockValues();
        }

        Masks.setCepMask(cepEditText);
        Masks.setCelularMask(celularEditText);

        btnEnviarProposta.setOnClickListener(onClickEnviarProposta());

        transferUtility = AWSCognitoUtil.getTransferUtility(this);

        viewDocumentoFrente = findViewById(R.id.layout_documento_frente);
        viewDocumentoFrente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedUploadAction = UPLOAD_DOCUMENTO_FRENTE;
                upload();
            }
        });
        viewDocumentoVerso = findViewById(R.id.layout_documento_verso);
        viewDocumentoVerso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedUploadAction = UPLOAD_DOCUMENTO_VERSO;
                upload();
            }
        });
        viewDocumentoEndereco = findViewById(R.id.layout_documento_endereco);
        viewDocumentoEndereco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedUploadAction = UPLOAD_DOCUMENTO_ENDERECO;
                upload();
            }
        });
        viewDocumentoAssinatura = findViewById(R.id.layout_documento_assinatura);
        viewDocumentoAssinatura.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedUploadAction = UPLOAD_DOCUMENTO_ASSINATURA;
                upload();
            }
        });

        findViewById(R.id.textview_termos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertAndroid.showMessageDialog(activity, getString(R.string.termos_proposta));
            }
        });

        findViewById(R.id.textview_ppe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertAndroid.showMessageDialog(activity, getString(R.string.termos_ppe));
            }
        });

    }

    @NonNull
    private View.OnClickListener onClickEnviarProposta() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doPostAccount();
            }
        };
    }

    private View.OnFocusChangeListener onFocusEstadoCivil = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(!hasFocus){
                return;
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.estado_civil);
            builder.setItems(civilStates, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    estadoCivilEditText.setText(civilStates[which]);
                    dataNascimentoEditText.requestFocus();
                    civilStateSelected = productDomainTO.civilStates.get(which).code;
                }
            });
            builder.show();
        }
    };

    private View.OnFocusChangeListener onFocusEstadoNascimento = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if(!hasFocus){
                return;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.estado_nascimento);
            builder.setItems(estados, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    birthdayEstateSelected = productDomainTO.states.get(which).code;
                    estadoNascimentoEditText.setText(birthdayEstateSelected);

                    cidadeNascimentoEditText.setEnabled(true);
                    cidadeNascimentoEditText.requestFocus();
                }
            });
            builder.show();
        }
    };

    private View.OnFocusChangeListener onFocusNomeBanco = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if(!hasFocus){
                return;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.banco);
            builder.setItems(banks, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    codeBankSelected = productDomainTO.banks.get(which).code;
                    nomeBancoEditText.setText(banks[which]);
                    codBancoEditText.setText(codeBankSelected);

                    agenciaEditText.requestFocus();
                }
            });
            builder.show();
        }
    };

    private View.OnFocusChangeListener onFocusTipoConta = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if(!hasFocus){
                return;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.tipo_conta);
            builder.setItems(accountTypes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    accountTypeSelected = productDomainTO.accountTypes.get(which).code;
                    tipoContaEditText.setText(accountTypes[which]);

                    agenciaEditText.requestFocus();
                }
            });
            builder.show();
        }
    };

    private View.OnFocusChangeListener onFocusEstadoDoc = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if(!hasFocus){
                return;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.estado_documento);
            builder.setItems(estados, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    estadoDocumentSelected = productDomainTO.states.get(which).code;
                    estadoDocEditText.setText(estadoDocumentSelected);

                    orgaoEmissorEditText.requestFocus();
                }
            });
            builder.show();
        }
    };

    private View.OnFocusChangeListener onFocusOrgaoEmissor = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if(!hasFocus){
                return;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.orgao_emissor);
            builder.setItems(documetIssuers, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    orgaoEmissorSelected = productDomainTO.documentIssuers.get(which).code;
                    orgaoEmissorEditText.setText(documetIssuers[which]);

                    dataEmissaoEditText.requestFocus();
                }
            });
            builder.show();
        }
    };

    private View.OnFocusChangeListener onFocusProfissao = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if(!hasFocus){
                return;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.profissao);
            builder.setItems(profissoes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    occupationSelected = productDomainTO.occupations.get(which).code;
                    profissaoEditText.setText(profissoes[which]);

                    cepEditText.requestFocus();
                }
            });
            builder.show();
        }
    };

    private View.OnFocusChangeListener onFocusCidadeNascimento = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if(!hasFocus){
                return;
            }


            progressDialog.setMessage(getString(R.string.busncando_cidades));
            progressDialog.show();

            GetProductDistricts.ResponseHandler rh = new GetProductDistricts.ResponseHandler() {
                @Override
                public void onSuccess(List<DistrictTO> data) {
                    progressDialog.dismiss();
                    districts = data;

                    if(districts == null || districts.size() == 0){
                        return;
                    }

                    cidades = labelsFromDistricts(data);

                    AlertDialog.Builder builder = new AlertDialog.Builder(activity);
                    builder.setTitle(R.string.cidade_nascimento);
                    builder.setItems(cidades, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            cidadeNascimentoEditText.setText(cidades[which]);
                            birthdayCitySelected = districts.get(which).code;
                            profissaoEditText.requestFocus();
                        }
                    });
                    builder.show();

                }

                @Override
                public void onUnsuccess(RetornoListaWS<DistrictTO> retornoWS) {
                    progressDialog.dismiss();
                    AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
                }

                @Override
                public void onError(IOException iOException) {
                    progressDialog.dismiss();
                    AlertAndroid.showMessageDialog(activity, iOException);
                }
            };

            GetProductDistricts.RequestParams rp = new GetProductDistricts.RequestParams();
            rp.state = birthdayEstateSelected;

            new GetProductDistricts().setRensponseHandler(rh).setRequestParams(rp).execute();

        }
    };

    View.OnClickListener onClickBuscarCep = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progressDialog.setMessage(getString(R.string.msg_buscando_cep));
            progressDialog.show();
            GetAccountAddress.ResponseHandler rh = new GetAccountAddress.ResponseHandler() {
                @Override
                public void onSuccess(AddressTO data) {
                    progressDialog.dismiss();
                    estadoEditText.setText(data.uf);
                    bairroEditText.setText(data.bairro);
                    ruaEditText.setText(data.logradouro);
                    cidadeEditText.setText(data.localidade);
                    complementoEditText.setText(data.complemento);
                    numeroEditText.requestFocus();
                }

                @Override
                public void onUnsuccess(RetornoWS<AddressTO> retornoWS) {
                    progressDialog.dismiss();
                    AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
                }

                @Override
                public void onError(IOException iOException) {
                    progressDialog.dismiss();
                    AlertAndroid.showMessageDialog(activity, iOException);
                }
            };

            GetAccountAddress.RequestParams rp = new GetAccountAddress.RequestParams();
            rp.customerId = UserPrefs.getProfile(activity).customerId;
            rp.code = Masks.unmask(cepEditText.getText().toString());

            new GetAccountAddress().setRensponseHandler(rh).setRequestParams(rp).execute();
        }
    };

    View.OnFocusChangeListener onFocusTipoDocumento = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if(!hasFocus){
                return;
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setTitle(R.string.tipo_documento);
            builder.setItems(tiposDocumento, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    tipoDcumento.setText(tiposDocumento[which]);
                    numeroDocEditText.requestFocus();
                    documentTypeSelected = productDomainTO.documentTypes.get(which).code;
                }
            });
            builder.show();

        }
    };

    private String [] labelsFromDomain(List<ProductDomainTO.DomainTO> domainTOList){
        List<String> labels = new ArrayList<>();
        for (ProductDomainTO.DomainTO domainTO : domainTOList) {
            labels.add(domainTO.description);
        }

        String [] lbls = new String[labels.size()];

        return labels.toArray(lbls);

    }

    private String [] labelsFromDistricts(List<DistrictTO> domainTOList){
        List<String> labels = new ArrayList<>();
        for (DistrictTO domainTO : domainTOList) {
            labels.add(domainTO.description);
        }

        String [] lbls = new String[labels.size()];

        return labels.toArray(lbls);

    }

    private void loadDomain(){

        progressDialog.setMessage(getString(R.string.msg_aguarde));
        progressDialog.show();

        GetDomain.ResponseHandler rh = new GetDomain.ResponseHandler() {
            @Override
            public void onSuccess(ProductDomainTO data) {
                productDomainTO = data;
                civilStates     = labelsFromDomain(data.civilStates);
                estados         = labelsFromDomain(data.states);
                profissoes      = labelsFromDomain(data.occupations);
                tiposDocumento  = labelsFromDomain(data.documentTypes);
                documetIssuers  = labelsFromDomain(data.documentIssuers);
                banks           = labelsFromDomain(data.banks);
                accountTypes    = labelsFromDomain(data.accountTypes);
                progressDialog.dismiss();
            }

            @Override
            public void onUnsuccess(RetornoWS<ProductDomainTO> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
            }

            @Override
            public void onError(IOException ioException) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, ioException);

            }
        };

        GetDomain.doCall(rh);
    }


    public void upload() {

        final CharSequence[] options = {getString(R.string.camera),
                getString(R.string.gallery), getString(R.string.cancel)};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(this);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    dialog.dismiss();
                    if (ContextCompat.checkSelfPermission(activity,
                            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(activity,
                                new String[]{Manifest.permission.CAMERA}, PICK_IMAGE_CAMERA);
                    } else {
                        launchCamera();
                    }
                } else if (item == 1) {
                    dialog.dismiss();
                    if (ContextCompat.checkSelfPermission(activity,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(activity,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, PICK_IMAGE_GALLERY);
                    } else {
                        launchGallery();
                    }
                } else {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    private void launchCamera() {

        photoPath = getExternalFilesDir(Environment.DIRECTORY_PICTURES) + "/invista_" + new Date().getTime() + ".jpg";
        File file = new File(photoPath);

        Uri photoUri;
        if (Build.VERSION.SDK_INT >= 24) {
            photoUri = FileProvider.getUriForFile(this, Constantes.FILE_PROVIDER, file);
        } else {
            photoUri = Uri.fromFile(file);
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra("android.intent.extras.CAMERA_FACING", 1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        intent.putExtra("outputFormat", "jpg");
        startActivityForResult(intent, PICK_IMAGE_CAMERA);

    }

    private void launchGallery() {

        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE_CAMERA) {

            try {

                Uri photoUri = Uri.fromFile(new File(photoPath));

                CropImageView.CropShape shape = CropImageView.CropShape.RECTANGLE;

                CropImage.activity(photoUri)
                        .setCropShape(shape)
                        .setInitialCropWindowPaddingRatio(0)
                        .setAllowFlipping(false)
                        .setFixAspectRatio(true)
                        .setGuidelines(CropImageView.Guidelines.OFF)
                        .setActivityTitle(getString(R.string.cortar_foto))
                        .start(activity);

            } catch (Exception e) {

                AlertAndroid.showMessageDialog(activity, getString(R.string.camera_error));

            }

        }

        if (resultCode == Activity.RESULT_OK && requestCode == PICK_IMAGE_GALLERY) {

            Uri photoUri = data.getData();

            CropImageView.CropShape shape = CropImageView.CropShape.RECTANGLE;

            CropImage.activity(photoUri)
                    .setCropShape(shape)
                    .setInitialCropWindowPaddingRatio(0)
                    .setAllowFlipping(false)
                    .setFixAspectRatio(true)
                    .setGuidelines(CropImageView.Guidelines.OFF)
                    .setActivityTitle(getString(R.string.cortar_foto))
                    .start(activity);
        }

        if (resultCode == Activity.RESULT_OK && requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            Uri imageUri = result.getUri();
            sourceFile = new File(imageUri.getPath());
            uploadFile(sourceFile);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PICK_IMAGE_CAMERA) {
            launchCamera();
        }
        if (requestCode == PICK_IMAGE_GALLERY) {
            launchGallery();
        }
    }

    private void uploadFile(File file) {

        progressDialog.setMessage(getString(R.string.msg_aguarde));
        progressDialog.show();

        IOUtils.compressPhoto(sourceFile, 750, 750);

        nomeEnviado = UserPrefs.getProfile(activity).customerId + selectedUploadAction;

        String extensao = "";
        if (file.getName().contains(".")) {
            extensao = file.getName().substring(file.getName().indexOf("."));
        }
        nomeEnviado += extensao;

        TransferObserver observer = transferUtility.upload("invista-public", nomeEnviado, file);

        observer.setTransferListener(new UploadListener());

    }

    private class UploadListener implements TransferListener {

        @Override
        public void onError(int id, Exception e) {

            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(activity, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(activity);
            }
            builder.setTitle(R.string.app_name)
                    .setMessage(activity.getString(R.string.send_fail))
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }

        @Override
        public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
        }

        @Override
        public void onStateChanged(int id, TransferState newState) {
            if (newState.equals(TransferState.COMPLETED)) {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                if (selectedUploadAction.equals(UPLOAD_DOCUMENTO_FRENTE)) {
                    hasDocumentoFrente = true;
                }
                if (selectedUploadAction.equals(UPLOAD_DOCUMENTO_VERSO)) {
                    hasDocumentoVerso = true;
                }
                if (selectedUploadAction.equals(UPLOAD_DOCUMENTO_ENDERECO)) {
                    hasDocumentoEndereco = true;
                }
                if (selectedUploadAction.equals(UPLOAD_DOCUMENTO_ASSINATURA)) {
                    hasDocumentoAssinatura = true;
                }
                new PostTransferDocument(nomeEnviado).execute();
            }

        }


    }

    private void mockValues(){
        cpfEditText.setText("834.047.530-46");
        nomeCompletoEditText.setText("Isolino Goncalve Ferreira");
        nomeMaeEditText.setText("marli goncalves");
        civilStateSelected = "C";
        dataNascimentoEditText.setText("11/08/1990");
        dataEmissaoEditText.setText("11/08/1990");
        dataVencimentoEditText.setText("11/08/1990");
        cidadeNascimentoEditText.setText("SERRA");

        birthdayEstateSelected = "ES";
        birthdayCitySelected = "0066";
        occupationSelected = "1";
        documentTypeSelected = "1";
        orgaoEmissorSelected = "01";

        numeroDocEditText.setText("3078222");

        estadoDocumentSelected = "ES";
        emailEditText.setText("isolinogf@gmail.com");
        cepEditText.setText("29.176-292");
        celularEditText.setText("(22) 99999-9898");
        numeroEditText.setText("23");
        codeBankSelected = "0001";
        accountTypeSelected = "1";
        agenciaEditText.setText("2121");
        numContaEditText.setText("11111");


    }

    public void doPostAccount(){

        PostAccountProposal.RensponseHandler rh = new PostAccountProposal.RensponseHandler() {
            @Override
            public void onSuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();

                AlertDialog.Builder confirmDialogBuilder = new AlertDialog.Builder(activity);

                confirmDialogBuilder.setCancelable(false)
                        .setTitle(R.string.app_name).setMessage(getString(R.string.proposta_sucesso))
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                AlertDialog alert = confirmDialogBuilder.create();
                alert.show();
            }

            @Override
            public void onUnsuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
            }

            @Override
            public void onError(IOException iOException) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, iOException);
            }
        };

        PostAccountProposal.RequestParams rp = new PostAccountProposal.RequestParams();
        rp.customerId = Masks.unmask(cpfEditText.getText().toString());
        rp.name = nomeCompletoEditText.getText().toString();
        rp.motherName = nomeMaeEditText.getText().toString();
        rp.gender = gender;//todo check
        rp.country = "BR";
        rp.civilStatus = civilStateSelected;
        rp.birthDate = FmtUtils.dateBRtoUS(dataNascimentoEditText.getText().toString());
        rp.birthCityCode = birthdayCitySelected;
        rp.birthCityDescription = cidadeNascimentoEditText.getText().toString();
        rp.birthState = birthdayEstateSelected;
        rp.documentType = documentTypeSelected;
        rp.documentNumber = numeroDocEditText.getText().toString();
        rp.documentIssuer = orgaoEmissorSelected;
        rp.documentState = estadoDocumentSelected;
        rp.documentEmission =  FmtUtils.dateBRtoUS(dataEmissaoEditText.getText().toString());
        rp.documentExpiration =  FmtUtils.dateBRtoUS(dataVencimentoEditText.getText().toString());
        rp.occupation = occupationSelected;
        rp.email = emailEditText.getText().toString();
        rp.zipCode = Masks.unmask(cepEditText.getText().toString());
        rp.city = cidadeEditText.getText().toString();
        rp.neighborhood = bairroEditText.getText().toString();
        rp.streetAddress = ruaEditText.getText().toString();
        rp.complement = complementoEditText.getText().toString();//safe
        rp.streetNumber = numeroEditText.getText().toString();
        rp.state = estadoEditText.getText().toString();
        rp.phone = Masks.unmask(celularEditText.getText().toString());
        rp.ppe = ppeSwitch.isChecked() ? "S" : "N";
        rp.bankCode = codeBankSelected;
        rp.bankAgency = agenciaEditText.getText().toString();
        rp.accountNumber = numContaEditText.getText().toString();
        rp.accountType = accountTypeSelected;
        rp.status = 1;


        boolean ok;

        ok = checkNull(rp.customerId, cpfEditText, getString(R.string.erro_cpf));
        ok = ok && checkNull(rp.name, nomeCompletoEditText, getString(R.string.erro_nome_completo));
        ok = ok && checkNull(rp.motherName, nomeMaeEditText, getString(R.string.erro_nome_mae));
        ok = ok && checkNull(rp.civilStatus, estadoCivilEditText, getString(R.string.erro_estado_civil));
        ok = ok && checkNull(rp.birthCityCode, cidadeNascimentoEditText, getString(R.string.erro_cidade_nascimento));
        ok = ok && checkNull(rp.birthCityDescription, cidadeNascimentoEditText, getString(R.string.erro_cidade_nascimento));
        ok = ok && checkNull(rp.birthState, estadoNascimentoEditText, getString(R.string.erro_estado_nascimento));
        ok = ok && checkNull(rp.documentType, tipoDcumento, getString(R.string.erro_tipo_documento));
        ok = ok && checkNull(rp.documentNumber, numeroDocEditText, getString(R.string.erro_num_doc));
        ok = ok && checkNull(rp.documentIssuer, orgaoEmissorEditText, getString(R.string.erro_orgao_emissor));
        ok = ok && checkNull(rp.documentEmission, dataEmissaoEditText, getString(R.string.erro_data_emissao));
        ok = ok && checkNull(rp.occupation, profissaoEditText, getString(R.string.erro_profisssao));
        ok = ok && checkNull(rp.email, emailEditText, getString(R.string.erro_email));
        ok = ok && checkNull(rp.zipCode, cepEditText, getString(R.string.erro_cep));
        ok = ok && checkNull(rp.city, cidadeEditText, getString(R.string.erro_cidade));
        ok = ok && checkNull(rp.streetAddress, ruaEditText, getString(R.string.erro_rua));
        ok = ok && checkNull(rp.streetNumber, numeroEditText, getString(R.string.erro_num_casa));
        ok = ok && checkNull(rp.state, estadoEditText, getString(R.string.erro_estado));
        ok = ok && checkNull(rp.phone, celularEditText, getString(R.string.erro_celular));
        ok = ok && checkNull(rp.bankCode, codBancoEditText, getString(R.string.erro_banco));
        ok = ok && checkNull(rp.bankAgency, agenciaEditText, getString(R.string.erro_agencia));
        ok = ok && checkNull(rp.accountNumber, numContaEditText, getString(R.string.erro_conta));
        ok = ok && checkNull(rp.accountType, tipoContaEditText, getString(R.string.erro_tipo_conta));

        if(ok){

            try {
                FmtUtils.DD_MM_YYYY.setLenient(false);
                FmtUtils.DD_MM_YYYY.parse(dataNascimentoEditText.getText().toString());
            } catch (ParseException e) {
                AlertAndroid.showMessageDialog(activity, getString(R.string.erro_data_nascimento_invalida));
                return;
            }

            try {
                FmtUtils.DD_MM_YYYY.parse(dataEmissaoEditText.getText().toString());
            } catch (ParseException e) {
                AlertAndroid.showMessageDialog(activity, getString(R.string.erro_data_emissao_invalida));
                return;
            }

            try {
                String dataVencimento = dataVencimentoEditText.getText().toString();
                if(!StringUtils.isBlank(dataVencimento)){
                    FmtUtils.DD_MM_YYYY.parse(dataVencimento);
                }
            } catch (ParseException e) {
                AlertAndroid.showMessageDialog(activity, getString(R.string.erro_data_vencimento_invalida));
                return;
            }

            if (!android.util.Patterns.EMAIL_ADDRESS.matcher(emailEditText.getText().toString()).matches()) {
                AlertAndroid.showMessageDialog(activity, getString(R.string.erro_email_invalido));
                return;
            }

            if (!hasDocumentoFrente) {
                AlertAndroid.showMessageDialog(activity, "Por favor, realize o upload do Documento (frente)");
                return;
            }

            if (!hasDocumentoVerso) {
                AlertAndroid.showMessageDialog(activity, "Por favor, realize o upload do Documento (verso)");
                return;
            }

            if (!hasDocumentoEndereco) {
                AlertAndroid.showMessageDialog(activity, "Por favor, realize o upload do Comprovante de Endereço");
                return;
            }

            if (!hasDocumentoAssinatura) {
                AlertAndroid.showMessageDialog(activity, "Por favor, realize o upload da sua Assinatura");
                return;
            }

            if (!termosSwitch.isChecked()) {
                AlertAndroid.showMessageDialog(activity, "Você precisa aceitar os termos de uso");
                return;
            }
        }

        if(ok){
            progressDialog.show();
            progressDialog.setMessage(getString(R.string.msg_enviando_proposta));
            new PostAccountProposal(rp).setRensponseHandler(rh).execute();
        }

    }

    private boolean checkNull(String value, EditText editText, String msg){
        if(StringUtils.isBlank(value)){
            AlertAndroid.showMessageDialog(activity, msg);
            return false;
        }
        return true;
    }


    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioMale:
                if (checked)
                    gender = "M";
                    break;
            case R.id.radioFemale:
                if (checked)
                    gender = "F";
                break;
        }
    }


//


}
