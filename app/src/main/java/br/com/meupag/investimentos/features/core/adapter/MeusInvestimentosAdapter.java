package br.com.meupag.investimentos.features.core.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.features.core.viewmodel.CoreViewModel;
import br.com.meupag.investimentos.features.results.view.ResultsActivity;
import br.com.meupag.investimentos.ws.transferobject.ExtractTO;
import br.com.meupag.investimentos.ws.transferobject.WalletTO;
import br.com.meupag.investimentos.util.FmtUtils;

public class MeusInvestimentosAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> items;
    private List<Object> copyItems = new ArrayList<>();
    private MeusInvestimentosAdapter.OnItemClickListener mOnItemClickListener;
    private CoreViewModel coreViewModel;

    private Context ctx;

    public MeusInvestimentosAdapter(List<Object> items, Context ctx) {
        this.items = items;
        this.copyItems.addAll(items);
        this.ctx = ctx;
    }

    public void updateCopyList(List<Object> titulos) {
        this.copyItems.clear();
        this.copyItems.addAll(titulos);
    }

    class FundoViewHolder extends RecyclerView.ViewHolder {
        protected TextView nome;
        protected TextView detalhe;
        protected TextView valorAplicado;
        protected TextView valorBruto;
        protected TextView valorBrutoLabel;
        protected TextView vencimento;
        protected TextView vencimentoLabel;
        protected TextView lucro;
        protected TextView lucroLabel;
        protected View item_fundo_parent;
        protected View item_clicavel;
        protected View item_fundo_parent_horizontal;
        protected View view;

        private FundoViewHolder(View v) {
            super(v);
            this.view = v;
            detalhe = v.findViewById(R.id.detalhe);
            item_fundo_parent = v.findViewById(R.id.item_fundo_detalhado_parent);
            item_fundo_parent_horizontal = v.findViewById(R.id.item_fundo_parent_horizontal);
            item_clicavel = v.findViewById(R.id.item_clicavel);
            vencimento = v.findViewById(R.id.vencimento);
            vencimentoLabel = v.findViewById(R.id.vencimento_label);
            valorAplicado = v.findViewById(R.id.valor_aplicado);
            valorBruto = v.findViewById(R.id.valor_bruto);
            valorBrutoLabel = v.findViewById(R.id.valor_bruto_label);
            lucro = v.findViewById(R.id.lucro);
            lucroLabel = v.findViewById(R.id.lucro_label);

        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_fundo_detalhado, parent, false);
        return new FundoViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        Object item = items.get(position);
        MeusInvestimentosAdapter.FundoViewHolder view = (MeusInvestimentosAdapter.FundoViewHolder) holder;

        if (item instanceof WalletTO.TituloTO) {
            final WalletTO.TituloTO tituloTO = (WalletTO.TituloTO)item;

            view.vencimento.setText(FmtUtils.dateUStoBR(tituloTO.dataVencimento));
            view.detalhe.setText(tituloTO.numeroTitulo + " - " + tituloTO.descricaoPapel);
            view.valorAplicado.setText(FmtUtils.realValue(tituloTO.valorPrincipal));
            view.valorBruto.setText(FmtUtils.realValue(tituloTO.valorResgateBruto));
            view.lucro.setText(FmtUtils.realValue(tituloTO.valorRendimento));
            view.item_clicavel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, tituloTO, position);
                    }
                }
            });

            view.item_fundo_parent_horizontal.setTag(tituloTO);

            view.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(ctx, ResultsActivity.class);
                    intent.putExtra(WalletTO.TituloTO.NAME, tituloTO);
                    ctx.startActivity(intent);
                }
            });
        } else if (item instanceof ExtractTO.MovimentacaoTO){
            final ExtractTO.MovimentacaoTO movimentacaoTO = (ExtractTO.MovimentacaoTO)item;

            view.vencimento.setText(FmtUtils.dateUStoBR(movimentacaoTO.dataOperacao));
            view.vencimentoLabel.setText(R.string.venda);
            view.detalhe.setText(movimentacaoTO.numTitulo + " - " + movimentacaoTO.descricaoProduto);
            view.valorAplicado.setText(FmtUtils.realValue(movimentacaoTO.valorPrincipal));

            boolean isPre = movimentacaoTO.remuneracao < 50.0;

            String remuneracaoPre = FmtUtils.percent(movimentacaoTO.remuneracao) + " ao Ano";
            String remuneracaoPos = FmtUtils.percent(movimentacaoTO.remuneracao) + " do CDI";
            view.valorBruto.setText(isPre ? remuneracaoPre :  remuneracaoPos);

            view.valorBrutoLabel.setText(R.string.remunera_o);
            view.lucro.setText(FmtUtils.realValue(movimentacaoTO.valorLiquido));
            view.lucroLabel.setText(R.string.resgate_liquido);
            view.item_clicavel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, movimentacaoTO, position);
                    }
                }
            });

            view.item_fundo_parent_horizontal.setTag(movimentacaoTO);
        }
    }

    public void filter(String text) {
        items.clear();
        if(text.isEmpty()){
            items.addAll(copyItems);
        } else{
            text = text.toLowerCase();
            for(Object item: copyItems){

                if(item instanceof WalletTO.TituloTO){
                    if(containsThisStringInAnyField(text, (WalletTO.TituloTO) item)){
                        items.add(item);
                    }
                }else{
                    if(containsThisStringInAnyField(text, (ExtractTO.MovimentacaoTO) item)){
                        items.add(item);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }

    private boolean containsThisStringInAnyField(String str, WalletTO.TituloTO t){
        return t.descricaoPapel.toLowerCase().contains(str)
                || FmtUtils.realValue(t.valorResgateLiquido).contains(str)
                || FmtUtils.realValue(t.valorPrincipal).contains(str)
                || FmtUtils.realValue(t.valorResgateBruto).contains(str)
                || t.numeroTitulo.toString().contains(str)
                || FmtUtils.dateUStoBR(t.dataVencimento).contains(str);
    }

    private boolean containsThisStringInAnyField(String str, ExtractTO.MovimentacaoTO m){
        return m.descricaoProduto.toLowerCase().contains(str)
                || FmtUtils.realValue(m.valorLiquido).contains(str)
                || FmtUtils.realValue(m.valorPrincipal).contains(str)
                || FmtUtils.realValue(m.valorBruto).contains(str)
                || m.numTitulo.contains(str)
                || FmtUtils.dateUStoBR(m.dataOperacao).contains(str);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public interface OnItemClickListener {
        void onItemClick(View view, WalletTO.TituloTO item, int position);
        void onItemClick(View view, ExtractTO.MovimentacaoTO item, int position);
    }

    public void setOnItemClickListener(final MeusInvestimentosAdapter.OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }
}
