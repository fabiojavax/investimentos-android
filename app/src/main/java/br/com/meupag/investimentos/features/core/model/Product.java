package br.com.meupag.investimentos.features.core.model;

public class Product {

    public Long productId;
    public String name;
    public Double investedAmount;
    public Double revenue;
    public Double currentAmount;
    public Double tax;

    public int color;

    public Product setColor(int color) {
        this.color = color;
        return this;
    }

    public Product setProductId(Long productId) {
        this.productId = productId;
        return this;
    }

    public Product setInvestedAmount(Double investedAmount) {
        this.investedAmount = investedAmount;
        return this;
    }

    public Product setRevenue(Double revenue) {
        this.revenue = revenue;
        return this;
    }

    public Product setCurrentAmount(Double currentAmount) {
        this.currentAmount = currentAmount;
        return this;
    }

    public Product setTax(Double tax) {
        this.tax = tax;
        return this;
    }

}
