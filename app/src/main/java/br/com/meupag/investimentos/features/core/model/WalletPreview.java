package br.com.meupag.investimentos.features.core.model;

import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.Observable;
import android.databinding.PropertyChangeRegistry;
import android.util.LongSparseArray;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.meupag.investimentos.ws.transferobject.WalletPreviewTO;
import br.com.meupag.investimentos.util.FmtUtils;

public class WalletPreview implements Observable {
    private PropertyChangeRegistry registry = new PropertyChangeRegistry();

    private Long customerId;
    private List<Product> products = new ArrayList<>(0);
    private Double totalInvestedAmount;
    private Double totalCurrentAmount;
    private Double totalRevenue;
    private Double totalTax;
    private String referenceDate;

    private LongSparseArray<Product> productMap;

    public WalletPreview() {
    }

    public WalletPreview(WalletPreviewTO walletPreviewTO) {
        this.customerId = Long.valueOf(walletPreviewTO.customerId);
        this.totalInvestedAmount = walletPreviewTO.totalInvestedAmount;
        this.totalCurrentAmount = walletPreviewTO.totalCurrentAmount;
        this.totalRevenue = walletPreviewTO.totalRevenue;
        this.totalTax = walletPreviewTO.totalTax;
        this.referenceDate = walletPreviewTO.referenceDate;

        if(walletPreviewTO.products != null){
            for (WalletPreviewTO.ProductTO productTO : walletPreviewTO.products) {
                Product product = new Product();
                product.setCurrentAmount(productTO.currentAmount);
                product.setInvestedAmount(productTO.investedAmount);
                product.setProductId(productTO.productId);
                product.setRevenue(productTO.revenue);
                product.setTax(productTO.tax);
                product.name = productTO.name;
                products.add(product);
            }
        }else{

            Product product = new Product();
            product.setCurrentAmount(0.0);
            product.setInvestedAmount(0.0);
            product.setProductId(1L);
            product.setRevenue(0.0);
            product.setTax(0.0);
            product.name = "";
            products.add(product);

            product = new Product();
            product.setCurrentAmount(0.0);
            product.setInvestedAmount(0.0);
            product.setProductId(2L);
            product.setRevenue(0.0);
            product.setTax(0.0);
            product.name = "";
            products.add(product);

        }

    }

    @Bindable
    public Double getTotalCurrentAmount() {
        return totalCurrentAmount;
    }

    @Bindable
    public String getTotalCurrentAmountString() {
        return FmtUtils.realValue(totalCurrentAmount);
    }

    @Bindable
    public Double getTotalRevenue() {
        return totalRevenue;
    }

    @Bindable
    public String getTotalRevenueString() {
        return FmtUtils.realValue(totalRevenue);
    }

    @Bindable
    public Double getTotalInvestedAmount() {
        return totalInvestedAmount;
    }

    @Bindable
    public String getTotalInvestedAmountString() {
        return FmtUtils.realValue(totalInvestedAmount);
    }

    public WalletPreview setTotalInvestedAmount(Double totalInvestedAmount) {
        this.totalInvestedAmount = totalInvestedAmount;
        return this;
    }

    public WalletPreview setTotalCurrentAmount(Double totalCurrentAmount) {
        this.totalCurrentAmount = totalCurrentAmount;
        return this;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }

    public void addProduct(Product product){
        products.add(product);
    }

    public void addAllProducts(List<Product> products){
        this.products.addAll(products);
    }

    public WalletPreview setCustomerId(Long customerId) {
        this.customerId = customerId;
        return this;
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        registry.remove(callback);
    }

    @BindingAdapter("android:text")
    public static void setText(TextView view, double value) {
        view.setText(String.valueOf(value));
    }

//    public void notifyChanges(){
//        registry.notifyChange(this, BR.totalCurrentAmount);
//        registry.notifyChange(this, BR.totalRevenue);
//        registry.notifyChange(this, BR.totalInvestedAmount);
//    }

    public Product getProductById(Long id){
        if(productMap == null){
            productMap = new LongSparseArray<>();
            for (Product product : products) {
                productMap.put(product.productId, product);
            }
        }
        
        return productMap.get(id);
    }
}
