package br.com.meupag.investimentos.features.core.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.databinding.FragmentCarteiraBinding;
import br.com.meupag.investimentos.features.core.model.Product;
import br.com.meupag.investimentos.features.core.model.WalletPreview;
import br.com.meupag.investimentos.features.core.viewmodel.CoreViewModel;
import br.com.meupag.investimentos.util.FmtUtils;
import br.com.meupag.investimentos.ws.transferobject.BalanceTO;

public class CarteiraFragment extends Fragment {

    private static final String TAG = CarteiraFragment.class.getSimpleName();

    public static final int[] COLORS = {
            ColorTemplate.rgb("33658A"),
            ColorTemplate.rgb("118AB2"),
            ColorTemplate.rgb("ffffff")
    };

    FragmentCarteiraBinding b;
    CoreViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_carteira, menu);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        b = DataBindingUtil.inflate(inflater, R.layout.fragment_carteira, container,
                false);
        return b.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpObserver();
    }

    private void setUpObserver() {
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            viewModel = ViewModelProviders.of(activity).get(CoreViewModel.class);

            viewModel.walletPreview.observe(activity, new Observer<WalletPreview>() {
                @Override
                public void onChanged(@Nullable WalletPreview walletPreview) {
                    updateScreenComponents();
                }
            });

            viewModel.balance.observe(activity, new Observer<BalanceTO>() {
                @Override
                public void onChanged(@Nullable BalanceTO balanceTO) {
                    updateScreenComponents();
                }
            });
        }
    }

    private void updateScreenComponents(){
        b.progressBar.setVisibility(View.INVISIBLE);
        WalletPreview walletPreview = viewModel.walletPreview.getValue();
        BalanceTO balanceTO = viewModel.balance.getValue();

        if(walletPreview != null && balanceTO != null){
            b.setWalletPreview(walletPreview);
            preparePieChart();
            loadPieChart();

            double totalCarteira = viewModel.walletPreview.getValue().getTotalCurrentAmount() + viewModel.balance.getValue().balance;
            b.piechartCurrentAmount.setText(FmtUtils.realValue(totalCarteira));

            b.piechartNomeRendimento.setVisibility(View.VISIBLE);
            b.centerLine.setVisibility(View.VISIBLE);
            b.piechartCurrentAmount.setVisibility(View.VISIBLE);
            b.piechartRevenue.setVisibility(View.VISIBLE);
            b.labelRendimento.setVisibility(View.VISIBLE);
        }
    }


    private void preparePieChart() {

        b.piechart.setDrawCenterText(false);
        b.piechart.animateY(500, Easing.EasingOption.EaseInOutQuart);
        b.piechart.setHoleColor(ColorTemplate.rgb("002c47"));
        b.piechart.setHoleRadius(90f);
        b.piechart.getDescription().setEnabled(false);
        b.piechart.getLegend().setEnabled(false);
        b.piechart.setDrawEntryLabels(false);

        b.piechart.setOnChartValueSelectedListener( new OnChartValueSelectedListener() {

            @Override
            public void onValueSelected(Entry e, Highlight h) {
                smoothChange((Product)e.getData());
            }

            @Override
            public void onNothingSelected() {
                Product p = new Product();
                p.currentAmount = viewModel.walletPreview.getValue().getTotalCurrentAmount() + viewModel.balance.getValue().balance;
                p.name = getString(R.string.valor_total);
                p.color = Color.WHITE;
                p.revenue = b.getWalletPreview().getTotalRevenue();

                smoothChange(p);
            }

        });

    }

    private void smoothChange(Product p){
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(150);
        fadeIn.setStartOffset(150);


        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator()); //and this
        fadeOut.setDuration(150);

        AnimationSet animation = new AnimationSet(false); //change to false
        animation.addAnimation(fadeOut);
        animation.addAnimation(fadeIn);

        b.piechartCurrentAmount.setAnimation(animation);
        b.piechartCurrentAmount.setText(FmtUtils.realValue(p.currentAmount));
        b.piechartCurrentAmount.setTextColor(p.color);

        b.piechartRevenue.setText(FmtUtils.realValue(p.revenue));
        b.piechartNomeRendimento.setText(p.name);
    }

    private void loadPieChart(){
        ArrayList<PieEntry> entries = new ArrayList<>();

        for (Product p : b.getWalletPreview().getProducts()) {
            entries.add(new PieEntry(p.currentAmount.floatValue(), p));
        }
        Product pSaldo =  new Product();
        pSaldo.name = getString(R.string.saldo);
        pSaldo.currentAmount = viewModel.balance.getValue().balance;
        pSaldo.color = COLORS[2];
        pSaldo.revenue = 0.0;
        pSaldo.productId = -1L;
        entries.add(new PieEntry(pSaldo.currentAmount.floatValue(), pSaldo));

        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setColors(COLORS);
        dataSet.setDrawValues(false);
        applyColorToEntryData(dataSet);

        b.piechart.setData(new PieData(dataSet));
        b.piechart.setVisibility(View.VISIBLE);

    }

    private void applyColorToEntryData(PieDataSet dataSet) {
        int idxColor = 0;
        for (int i = 0; i < dataSet.getValues().size(); i++) {
            int color = dataSet.getColors().get(idxColor);
            ((Product)dataSet.getValues().get(i).getData()).setColor(color);

            idxColor = i == dataSet.getColors().size()-1 ? 0 : idxColor+1;
        }
    }



}
