package br.com.meupag.investimentos.features.core.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.Room;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.databinding.ActivityCoreBinding;
import br.com.meupag.investimentos.features.core.adapter.CoreActivityAdapter;
import br.com.meupag.investimentos.features.core.model.WalletPreview;
import br.com.meupag.investimentos.features.core.viewmodel.CoreViewModel;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.storage.db.AppDatabase;
import br.com.meupag.investimentos.storage.db.DomainDAO;
import br.com.meupag.investimentos.storage.model.Domain;
import br.com.meupag.investimentos.util.ActivityUtils;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.FmtUtils;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.widget.NonSwipeableViewPager;
import br.com.meupag.investimentos.ws.call.GetCustomerBalance;
import br.com.meupag.investimentos.ws.call.GetCustomerProfile;
import br.com.meupag.investimentos.ws.call.GetCustomerWalletPreview;
import br.com.meupag.investimentos.ws.call.GetDomain;
import br.com.meupag.investimentos.ws.call.PutCustomerToken;
import br.com.meupag.investimentos.ws.transferobject.BalanceTO;
import br.com.meupag.investimentos.ws.transferobject.ProductDomainTO;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;
import br.com.meupag.investimentos.ws.transferobject.WalletPreviewTO;

import static br.com.meupag.investimentos.Constantes.DB_INVISTA;

public class CoreActivity extends AppCompatActivity {

    String TAG = CoreActivity.class.getSimpleName();
    Activity activity = this;

    ActionBar actionBar;
    ActivityCoreBinding b;
    CoreViewModel coreViewModel;
    ProgressDialog progressDialog;

    GetDomain getDomain;

    DomainDAO domainDAO;
    AppDatabase appDatabase;

    GetCustomerWalletPreview getCustomerWalletPreview;
    GetCustomerBalance getCustomerBalance;

    Map<Integer, ViewPager> viewPagerMap = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = DataBindingUtil.setContentView(this, R.layout.activity_core);

        configViewModel();
        configViewPager();
        configNavigation();
        configToobar();

        prepareDAOs();

        loadCustomerProfile();
        loadWalletAndBalance();
        loadDomain();
        putTokenDevice();
    }

    private void configViewModel() {
        coreViewModel = ViewModelProviders.of(this).get(CoreViewModel.class);
        coreViewModel.profile.observe(this, new Observer<ProfileTO>() {
            @Override
            public void onChanged(@Nullable ProfileTO profileTO) {
                if(profileTO != null){
                    UserPrefs.setProfile(activity, profileTO);
                }
            }
        });

        coreViewModel.balance.observe(this, new Observer<BalanceTO>() {
            @Override
            public void onChanged(@Nullable BalanceTO balanceTO) {
                if(balanceTO != null){
                    showSaldo(balanceTO.balance);
                }
            }
        });
    }
    private void configViewPager() {
        CoreActivityAdapter adapter = new CoreActivityAdapter(getSupportFragmentManager());
        adapter.addFragment(new CarteiraFragment(), getResources().getString(R.string.app_name));
        adapter.addFragment(new InvestirFragment(), getResources().getString(R.string.investir));
        adapter.addFragment(new MeusInvestimentosFragment(), getResources().getString(R.string.resultados));
        adapter.addFragment(new ProfileFragment(), getResources().getString(R.string.perfil));

        b.viewpager.setAdapter(adapter);
        b.viewpager.setOffscreenPageLimit(3);

        b.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {}
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                ViewPager viewPager = null;
                ActivityUtils.hideKeyboard(CoreActivity.this);
                switch (position) {
                    case 0:
                        b.navigation.setSelectedItemId(R.id.carteira);
                        viewPager = viewPagerMap.get(R.id.carteira);
                        actionBar.setTitle(R.string.carteira);
                        break;

                    case 1:
                        b.navigation.setSelectedItemId(R.id.invertir);
                        viewPager = viewPagerMap.get(R.id.invertir);
                        actionBar.setTitle(R.string.investir);
                        break;

                    case 2:
                        b.navigation.setSelectedItemId(R.id.investimentos);
                        String customerId = UserPrefs.getProfile(activity).customerId;
                        viewPager = viewPagerMap.get(R.id.investimentos);
                        actionBar.setTitle(R.string.investimentos);
                        break;

                    case 3:
                        b.navigation.setSelectedItemId(R.id.perfil);
                        viewPager = viewPagerMap.get(R.id.perfil);
                        actionBar.setTitle(R.string.perfil);
                        break;
                }

                if(viewPager != null){
                    b.tabs.setupWithViewPager(viewPager);
                    b.tabs.setVisibility(View.VISIBLE);

                }else{
                    b.tabs.setVisibility(View.GONE);
                }
            }
        });
    }
    private void configNavigation() {
        b.navigation.enableAnimation(false);
        b.navigation.enableShiftingMode(false);
        b.navigation.enableItemShiftingMode(false);
        b.navigation.setTextVisibility(false);
        b.navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.carteira:
                        b.viewpager.setCurrentItem(0);
                        break;
                    case R.id.invertir:
                        b.viewpager.setCurrentItem(1);
                        break;
                    case R.id.investimentos:
                        b.viewpager.setCurrentItem(2);
                        break;
                    case R.id.perfil:
                        b.viewpager.setCurrentItem(3);
                        break;
                }
                return true;
            }
        });
    }
    private void configToobar() {
        setSupportActionBar(b.toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(R.string.carteira);
    }

    private void prepareDAOs() {
        appDatabase = Room.databaseBuilder(activity, AppDatabase.class, DB_INVISTA).build();
        domainDAO = appDatabase.getDomainDAO();
    }

    public void loadCustomerProfile(){
        final GetCustomerProfile.ResponseHandler rh = new GetCustomerProfile.ResponseHandler() {
            @Override
            public void onSuccess(ProfileTO data) {
                coreViewModel.profile.setValue(data);
            }

            @Override
            public void onError(RetornoWS<ProfileTO> retornoWS) {
                coreViewModel.profile.setValue(null);
            }
        };

        GetCustomerProfile getCustomerProfile = new GetCustomerProfile();
        getCustomerProfile.setRensponseHandler(rh);
        getCustomerProfile.requestParams.customerId = UserPrefs.getProfile(this).customerId;
        getCustomerProfile.execute();
    }
    public void loadWalletAndBalance() {
        String customerId = UserPrefs.getProfile(this).customerId;
        coreViewModel.balance.setValue(null);
        coreViewModel.walletPreview.setValue(null);

        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.msg_carregando_dados));
        progressDialog.setCancelable(false);
        progressDialog.show();

        loadWalletPreview(customerId);
        loadCustomerBalance(customerId);
    }
    public void loadWalletPreview(String customerId){
        final GetCustomerWalletPreview.ResponseHandler rh = new GetCustomerWalletPreview.ResponseHandler() {
            @Override
            public void onSuccess(RetornoWS<WalletPreviewTO> retornoWS) {
                coreViewModel.walletPreview.setValue(new WalletPreview(retornoWS.getData()));
                getCustomerWalletPreview = null;

                if(getCustomerBalance == null){
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onUnsuccess(RetornoWS<WalletPreviewTO> retornoWS) {
                coreViewModel.walletPreview.setValue(null);
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
            }

            @Override
            public void onError(IOException iOException) {
                progressDialog.dismiss();
                getCustomerWalletPreview = null;

                if(getCustomerBalance != null){
                    AlertAndroid.showMessageDialog(activity, iOException);
                }
            }
        };

        if(getCustomerWalletPreview != null){
            getCustomerWalletPreview.cancel(true);
        }

        getCustomerWalletPreview = new GetCustomerWalletPreview();
        getCustomerWalletPreview.requestParams.customerId = customerId;
        getCustomerWalletPreview.setResponseHandler(rh);
        getCustomerWalletPreview.execute();

    }
    public void loadCustomerBalance(String customerId){
        final GetCustomerBalance.ResponseHandler rh = new GetCustomerBalance.ResponseHandler() {
            @Override
            public void onSuccess(BalanceTO data) {
                coreViewModel.balance.setValue(data);
                UserPrefs.setBalance(activity, data != null ? data.balance : 0.0);
                getCustomerBalance = null;

                if(getCustomerWalletPreview == null){
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onUnsuccess(RetornoWS<BalanceTO> retornoWS) {
                coreViewModel.balance.setValue(null);
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
            }

            @Override
            public void onError(IOException iOException) {
                progressDialog.dismiss();
                getCustomerBalance = null;

                if(getCustomerWalletPreview != null){
                    AlertAndroid.showMessageDialog(activity, iOException);
                }

            }

        };

        if(getCustomerBalance != null){
            getCustomerBalance.cancel(true);
        }

        getCustomerBalance = new GetCustomerBalance();
        getCustomerBalance.setRensponseHandler(rh);
        getCustomerBalance.requestParams.customerId = customerId;
        getCustomerBalance.execute();
    }
    private void loadDomain(){
        if(getDomain == null) {
            getDomain = new GetDomain();
            getDomain.setResponseHandler(new GetDomain.ResponseHandler() {
                @Override
                public void onSuccess(ProductDomainTO data) {
                    new UpdateTableDomain().execute(data);
                }

                @Override
                public void onUnsuccess(RetornoWS<ProductDomainTO> retornoWS) {
                    progressDialog.dismiss();
                    AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
                }

                @Override
                public void onError(IOException ioException) {
                    progressDialog.dismiss();
                    AlertAndroid.showMessageDialog(activity, ioException);

                }
            });
            getDomain.execute();
        }
    }
    private void putTokenDevice() {
        PutCustomerToken.RequestParams rp = new PutCustomerToken.RequestParams();
        rp.customerId = UserPrefs.getProfile(activity).customerId;
        rp.deviceToken = PreferenceManager.getDefaultSharedPreferences(this)
                .getString("PREF_USER_TOKEN", null);

        PutCustomerToken.doCall(activity, rp, new PutCustomerToken.ResponseHandler() {
            @Override
            public void onSuccess(RetornoWS<Object> retornoWS) {

            }

            @Override
            public void onUnsuccess(RetornoWS<Object> retornoWS) {

            }

            @Override
            public void onError(IOException iOException) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_refresh){
            loadWalletAndBalance();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void showSaldo(@NonNull Double saldo) {
        String saldoValor = getString(R.string.saldo_valor);
        saldoValor += FmtUtils.realValue(saldo);
        actionBar.setSubtitle(saldoValor);
    }

    public void configTabs(@NonNull NonSwipeableViewPager viewPager, int menuIem){
        viewPagerMap.put(menuIem, viewPager);
    }

    @Override
    public void onBackPressed() {
        if (b.viewpager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            b.viewpager.setCurrentItem(0);
        }
    }

    private class UpdateTableDomain extends AsyncTask<ProductDomainTO, Void, Boolean>{

        @Override
        protected Boolean doInBackground(ProductDomainTO... productDomainTOS) {

            ProductDomainTO data = productDomainTOS[0];

            if(data == null){
                return false;
            }

            for (ProductDomainTO.DomainTO bankDomainTO : data.banks) {
                persistOrUpdateDomainItem(bankDomainTO, Domain.BANK_TYPE);
            }

            for (ProductDomainTO.DomainTO accountType : data.accountTypes) {
                persistOrUpdateDomainItem(accountType, Domain.ACCOUNT_TYPE);
            }

            return true;
        }

        private void persistOrUpdateDomainItem(ProductDomainTO.DomainTO bankDomainTO, int domainType) {
            Domain persistedDomain = domainDAO.getDomainByTypeWithCodigo(domainType, bankDomainTO.code);
            if(persistedDomain == null){
                domainDAO.insert(new Domain(bankDomainTO, domainType));
            }else{
                persistedDomain.setDescription(bankDomainTO.description);
                domainDAO.update(persistedDomain);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        loadWalletAndBalance();

        int currItem = b.viewpager.getCurrentItem();
        CoreActivityAdapter adapter = (CoreActivityAdapter)b.viewpager.getAdapter();

        if(adapter != null){
            Fragment fragment = adapter.getItem(currItem);
            if(fragment instanceof MeusInvestimentosFragment){
                ((MeusInvestimentosFragment) fragment).loadCustomerExtract();
                ((MeusInvestimentosFragment) fragment).loadCustomerWallet();
            }
        }
    }
}