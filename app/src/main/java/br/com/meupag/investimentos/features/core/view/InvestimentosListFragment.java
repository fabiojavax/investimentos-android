package br.com.meupag.investimentos.features.core.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.features.core.adapter.MeusInvestimentosAdapter;

public class InvestimentosListFragment extends Fragment {

    RecyclerView recyclerView;
    MeusInvestimentosAdapter adapter;
    List<Object> data = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_list, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL));


        adapter = new MeusInvestimentosAdapter(data, getActivity());
        recyclerView.setAdapter(adapter);
        return view;
    }


    public void updateTitulos(List<Object> titulos) {
        View view = getView();
        view.findViewById(R.id.msg_nao_possui_invesimentos).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.msg_nao_resgatou_nada).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.recycler_view).setVisibility(View.VISIBLE);


        data.clear();
        data.addAll(titulos);
        adapter.updateCopyList(titulos);
        adapter.notifyDataSetChanged();
    }

    public void showNoInvestiments(int msgToShow) {
        View view = getView();
        view.findViewById(R.id.msg_nao_possui_invesimentos).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.msg_nao_resgatou_nada).setVisibility(View.INVISIBLE);
        view.findViewById(R.id.recycler_view).setVisibility(View.INVISIBLE);

        view.findViewById(msgToShow).setVisibility(View.VISIBLE);

    }

    public void filter(String text){
        adapter.filter(text);
    }
}
