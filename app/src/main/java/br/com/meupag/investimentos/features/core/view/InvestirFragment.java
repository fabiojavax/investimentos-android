package br.com.meupag.investimentos.features.core.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.databinding.FragmentInvestirBinding;
import br.com.meupag.investimentos.features.core.viewmodel.CoreViewModel;
import br.com.meupag.investimentos.features.simulador.view.SimuladorActivity;
import br.com.meupag.investimentos.features.core.model.WalletPreview;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.ws.transferobject.BalanceTO;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;

import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.SALDO;
import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.STOCK_TYPE;
import static br.com.meupag.investimentos.ws.transferobject.StockRatesTO.POS_FIXADO;
import static br.com.meupag.investimentos.ws.transferobject.StockRatesTO.PRE_FIXADO;

public class InvestirFragment extends Fragment {

    public static final String TAG = InvestirFragment.class.getSimpleName();

    CoreViewModel viewModel;

    FragmentInvestirBinding b;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        b = DataBindingUtil.inflate(inflater, R.layout.fragment_investir, container,
                true);
        b.preFixado.setOnClickListener(onSelectPreFixado);
        b.posFixado.setOnClickListener(onSelectPosFixado);
        return b.getRoot();
    }

    private void setUpObserver() {
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            viewModel = ViewModelProviders.of(activity).get(CoreViewModel.class);
            viewModel.profile.observe(activity, new Observer<ProfileTO>() {
                @Override
                public void onChanged(@Nullable ProfileTO profileTO) {
                    updateScreenComponents();
                }
            });
            updateScreenComponents();
        }
    }

    private void updateScreenComponents(){
        ProfileTO profileTO = viewModel.profile.getValue();
        if(profileTO != null){
            b.banco.setText(R.string.cod_banco_cfi);
            b.agencia.setText(R.string.cod_agencia_cfi);
            String account = UserPrefs.getPrefAccountNumberCfi(getActivity());
            account = new StringBuffer(account).insert(account.length()-2, "-").toString();
            b.conta.setText(account);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpObserver();
    }

    private void abrirSimulador(int stockType){
        Intent intent = new Intent(getActivity(), SimuladorActivity.class);

        BalanceTO balanceTO = viewModel.balance.getValue();
        if(balanceTO != null){
            intent.putExtra(SALDO, balanceTO.balance);
        }
        intent.putExtra(STOCK_TYPE, stockType);
        startActivity(intent);
    }

    private View.OnClickListener onSelectPosFixado = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            abrirSimulador(POS_FIXADO);
        }
    };

    private View.OnClickListener onSelectPreFixado = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            abrirSimulador(PRE_FIXADO);
        }
    };
}
