package br.com.meupag.investimentos.features.core.view;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.databinding.FragmentMeusInvestimentosBinding;
import br.com.meupag.investimentos.features.core.viewmodel.CoreViewModel;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.call.GetCustomerExtract;
import br.com.meupag.investimentos.ws.call.GetCustomerWallet;
import br.com.meupag.investimentos.ws.transferobject.ExtractTO;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;
import br.com.meupag.investimentos.ws.transferobject.WalletTO;

public class MeusInvestimentosFragment extends Fragment {

    static int COMPRADOS = 0;
    static int RESGATADOS = 1;

    FragmentMeusInvestimentosBinding b;
    ViewPagerAdapter viewPagerAdapter;
    SearchView searchView;

    List<Object> resgatados = new ArrayList<>();
    List<Object> comprados = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        CoreActivity coreActivity = (CoreActivity) getActivity();
        if(coreActivity != null){
            viewPagerAdapter = new ViewPagerAdapter(getChildFragmentManager());
            viewPagerAdapter.addFragment(new InvestimentosListFragment());
            viewPagerAdapter.addFragment(new InvestimentosListFragment());
            b.viewPager.setAdapter(viewPagerAdapter);
            b.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    getActivity().invalidateOptionsMenu();
                    if(position == COMPRADOS){
                        ((InvestimentosListFragment) viewPagerAdapter.getItem(RESGATADOS)).filter("");
                    }else{
                        ((InvestimentosListFragment) viewPagerAdapter.getItem(COMPRADOS)).filter("");
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            coreActivity.configTabs(b.viewPager, R.id.investimentos);

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
                loadCustomerWallet();
                loadCustomerExtract();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_meus_investimentos, menu);

        Activity act = getActivity();

        if(act != null){

            SearchManager sm = (SearchManager) act.getSystemService(Context.SEARCH_SERVICE);

            if(sm != null){
                MenuItem searchItem = menu.findItem(R.id.search);

                searchView = (SearchView) searchItem.getActionView();
                searchView.setQueryHint(getString(R.string.buscar_data_valor));
                searchView.setSearchableInfo(sm.getSearchableInfo(act.getComponentName()));

                searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText) {
                        filterFragmentList(newText);
                        return true;
                    }
                });
            }
        }
    }

    public void filterFragmentList(String newText) {
        int currentPage = b.viewPager.getCurrentItem();
        Fragment currentFragment = viewPagerAdapter.getItem(currentPage);
        ((InvestimentosListFragment) currentFragment).filter(newText);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        b = DataBindingUtil.inflate(inflater, R.layout.fragment_meus_investimentos, container,
                false);

        b.swipe.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        getActivity().invalidateOptionsMenu();
                        int currentPage = b.viewPager.getCurrentItem();
                        if(currentPage == COMPRADOS){
                            loadCustomerWallet();
                        }else{
                            loadCustomerExtract();
                        }
                    }
                }
        );

        return b.getRoot();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {

        List<InvestimentosListFragment> frags = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return frags.get(position);
        }

        @Override
        public int getCount() {
            return frags.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0) {
                title = getString(R.string.comprados);
            } else if (position == 1) {
                title = getString(R.string.resgatados);
            }
            return title;
        }

        public void addFragment(InvestimentosListFragment frag){
            frags.add(frag);
        }


    }

    public void loadCustomerWallet(){
        b.swipe.setRefreshing(true);
        final GetCustomerWallet.ResponseHandler rh = new GetCustomerWallet.ResponseHandler() {
            @Override
            public void onSuccess(WalletTO data) {
                b.swipe.setRefreshing(false);
                InvestimentosListFragment curFrag = (InvestimentosListFragment) viewPagerAdapter.getItem(COMPRADOS);

                if(data != null){
                    comprados = new ArrayList<Object>(data.titulos.titulo);
                    curFrag.updateTitulos(comprados);
                }else{
                    curFrag.showNoInvestiments(R.id.msg_nao_possui_invesimentos);
                }
            }

            @Override
            public void onUnsuccess(RetornoWS<WalletTO> retornoWS) {
                b.swipe.setRefreshing(false);
                AlertAndroid.showMessageDialog(getActivity(), retornoWS.getMessage());
            }

            @Override
            public void onError(IOException iOException) {
                b.swipe.setRefreshing(false);
                AlertAndroid.showMessageDialog(getActivity(), iOException);
            }
        };

        ProfileTO profileTO = UserPrefs.getProfile(getContext());

        GetCustomerWallet getCustomerWallet = new GetCustomerWallet();
        getCustomerWallet.requestParams.customerId = profileTO.customerId;
        getCustomerWallet.setRensponseHandler(rh);
        getCustomerWallet.execute();
    }

    public void loadCustomerExtract(){
        b.swipe.setRefreshing(true);
        final GetCustomerExtract.ResponseHandler rh = new GetCustomerExtract.ResponseHandler() {
            @Override
            public void onSuccess(ExtractTO data) {
                b.swipe.setRefreshing(false);
                InvestimentosListFragment curFrag = (InvestimentosListFragment) viewPagerAdapter.getItem(RESGATADOS);

                if(data != null){
                    resgatados = new ArrayList<Object>(data.movimentacoes.movimentacaoCarteira);
                    curFrag.updateTitulos(resgatados);
                }else{
                    curFrag.showNoInvestiments(R.id.msg_nao_resgatou_nada);
                }
            }

            @Override
            public void onUnsuccess(RetornoWS<ExtractTO> retornoWS) {
                b.swipe.setRefreshing(false);
                AlertAndroid.showMessageDialog(getActivity(), retornoWS.getMessage());
            }

            @Override
            public void onError(IOException ioException) {
                b.swipe.setRefreshing(false);
                AlertAndroid.showMessageDialog(getActivity(), ioException);
            }
        };

        ProfileTO profileTO = UserPrefs.getProfile(getContext());

        GetCustomerExtract getCustomerExtract = new GetCustomerExtract();
        getCustomerExtract.requestParams.customerId = profileTO.customerId;
        getCustomerExtract.setRensponseHandler(rh);
        getCustomerExtract.execute();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        loadCustomerWallet();
        ((CoreActivity)getActivity()).loadWalletAndBalance();
    }
}
