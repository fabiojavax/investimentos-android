package br.com.meupag.investimentos.features.core.view;


import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.com.meupag.investimentos.BuildConfig;
import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.features.core.viewmodel.CoreViewModel;
import br.com.meupag.investimentos.features.login.view.LoginActivity;
import br.com.meupag.investimentos.features.profile.view.ProfileActivity;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.ws.transferobject.BalanceTO;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;

import static br.com.meupag.investimentos.features.profile.view.ProfileActivity.CARDVIEW_TO_SHOW;
import static br.com.meupag.investimentos.features.profile.view.ProfileActivity.SALDO_PARAM;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private static final String TAG = ProfileFragment.class.getSimpleName();

    private static final int ALTER_PROFILE_CODE = 0;
    private static final int ALTER_PASSWORD_CODE = 1;
    private static final int ALTER_SIGNATURE_CODE = 2;
    private static final int REQUEST_TED_CODE = 3;

    View nomeEmailTel;
    View senha;
    View assinatura;
    View dadosBancarios;
    View solicitarTed;
    View sair;

    TextView userProfileNameTextView;
    TextView userProfileEmailTextView;

    CoreViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_profile, container, false);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setUpObserver();
    }



    private void setUpObserver() {
        final FragmentActivity activity = getActivity();
        if (activity != null) {
            View view = getView();
            if(view != null) {

                viewModel = ViewModelProviders.of(activity).get(CoreViewModel.class);

                Log.d(TAG, "Configurando observer");

                nomeEmailTel = view.findViewById(R.id.mrl_nome_email_tel);
                nomeEmailTel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), ProfileActivity.class);
                        intent.putExtra(CARDVIEW_TO_SHOW, R.id.ll_nome_email_tel);
                        startActivityForResult(intent, ALTER_PROFILE_CODE);
                    }
                });

                senha = view.findViewById(R.id.mrl_senha);
                senha.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), ProfileActivity.class);
                        intent.putExtra(CARDVIEW_TO_SHOW, R.id.ll_senha);
                        startActivityForResult(intent, ALTER_PASSWORD_CODE);
                    }
                });

                dadosBancarios = view.findViewById(R.id.mrl_dados_bancarios);
                dadosBancarios.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), ProfileActivity.class);
                        intent.putExtra(CARDVIEW_TO_SHOW, R.id.ll_dados_bancarios);
                        startActivity(intent);
                    }
                });

                solicitarTed = view.findViewById(R.id.mrl_solicitar_ted);
                solicitarTed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), ProfileActivity.class);
                        intent.putExtra(CARDVIEW_TO_SHOW, R.id.ll_solicitar_ted);

                        BalanceTO balanceTO = viewModel.balance.getValue();
                        if(balanceTO != null){
                            intent.putExtra(SALDO_PARAM, balanceTO.balance);
                        }
                        startActivityForResult(intent, REQUEST_TED_CODE);
                    }
                });


                sair = view.findViewById(R.id.mrl_sair);
                sair.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final Activity activity = getActivity();

                        AlertDialog.Builder adb = new AlertDialog.Builder(activity);
                        adb.setMessage(R.string.logout_confirmation);
                        adb.setIcon(R.drawable.ic_exit_to_app_white_24dp);
                        adb.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                UserPrefs.clear(activity);
                                Intent intent = new Intent(getContext(), LoginActivity.class);
                                startActivity(intent);
                                activity.finish();
                            }
                        });
                        adb.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            } });
                        adb.show();
                    }
                });


                userProfileEmailTextView = view.findViewById(R.id.tv_user_profile_email);
                userProfileNameTextView = view.findViewById(R.id.tv_user_profile_name);

                viewModel.profile.observe(activity, new Observer<ProfileTO>() {
                    @Override
                    public void onChanged(@Nullable ProfileTO profileTO) {
                        if(profileTO != null){
                            userProfileNameTextView.setText(profileTO.name);
                            userProfileEmailTextView.setText(profileTO.email);
                        }
                    }
                });

                ((TextView)view.findViewById(R.id.version_number)).setText("Version " + BuildConfig.VERSION_NAME);

            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        CoreActivity coreActivity = (CoreActivity) getActivity();
        if(coreActivity != null && resultCode == Activity.RESULT_OK){
            ProfileTO profileTO = UserPrefs.getProfile(coreActivity);
            userProfileNameTextView.setText(profileTO.name);
            userProfileEmailTextView.setText(profileTO.email);
            coreActivity.loadWalletAndBalance();
        }
    }
}
