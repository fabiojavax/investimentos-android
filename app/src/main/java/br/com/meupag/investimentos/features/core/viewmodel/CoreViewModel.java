package br.com.meupag.investimentos.features.core.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import br.com.meupag.investimentos.features.core.model.Product;
import br.com.meupag.investimentos.features.core.model.WalletPreview;
import br.com.meupag.investimentos.ws.transferobject.BalanceTO;
import br.com.meupag.investimentos.ws.transferobject.ExtractTO;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;
import br.com.meupag.investimentos.ws.transferobject.WalletTO;

public class CoreViewModel extends ViewModel {

    public final MutableLiveData<WalletPreview> walletPreview = new MutableLiveData<>();
    public final MutableLiveData<WalletTO> wallet = new MutableLiveData<>();
    public final MutableLiveData<ProfileTO> profile = new MutableLiveData<>();
    public final MutableLiveData<ExtractTO> extract = new MutableLiveData<>();
    public final MutableLiveData<BalanceTO> balance = new MutableLiveData<>();

    public Product getProductById(Long codigoPapel) {
        WalletPreview w = walletPreview.getValue();
        return w == null ? null : w.getProductById(codigoPapel);
    }
}
