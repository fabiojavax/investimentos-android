package br.com.meupag.investimentos.features.credentials.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.call.PostAccountCredentials;

public class CredentialsActivity extends AppCompatActivity {

    private static final String TAG = CredentialsActivity.class.getSimpleName();
    Activity activity = this;

    ProgressDialog progressDialog;

    Iterator<Fragment> fragmentIterator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credentials);

        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new FirstStepFragment());
        fragments.add(new SecondStepFragment());
        fragments.add(new ThirdStepFragment());
        fragments.add(new FourthStepFragment());
        fragments.add(new FifthStepFragment());

        fragmentIterator = fragments.iterator();

        addFragment(fragmentIterator.next());

    }

    public void nextStep(){
        if(fragmentIterator.hasNext()){
            replaceFragment(fragmentIterator.next());
        }else{
            doPostCredentials();
        }
    }


    protected void addFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.frame_container, fragment)
                .disallowAddToBackStack()
                .commit();
    }

    protected void replaceFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_container, fragment)
                .commit();
    }

    private void doPostCredentials(){
        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.msg_definindo_credentiais));
        progressDialog.setCancelable(false);
        progressDialog.show();


        PostAccountCredentials.ResponseHandler rh = new PostAccountCredentials.ResponseHandler() {
            @Override
            public void onSuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showConfirmDialog(activity, retornoWS.getMessage(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            }

            @Override
            public void onUnsuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
            }

            @Override
            public void onError(IOException iOException) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, iOException);
            }
        };

        PostAccountCredentials.RequestParams rp = new PostAccountCredentials.RequestParams();
        rp.customerId = UserPrefs.getProfile(activity).customerId;
        rp.signature = UserPrefs.getSignature(activity);
        rp.password = UserPrefs.getPassword(activity);

        new PostAccountCredentials().setRequestParams(rp).setResponseHandler(rh).execute();
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "Blocking back behavior");
        finish();
    }
}
