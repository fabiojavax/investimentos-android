package br.com.meupag.investimentos.features.credentials.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import br.com.meupag.investimentos.BuildConfig;
import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.storage.UserPrefs;

public class FifthStepFragment extends Fragment {

    private static final String TAG = FifthStepFragment.class.getSimpleName();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fifth_step_credencials, null);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final CredentialsActivity act = (CredentialsActivity) getActivity();
        if(act != null){
            act.findViewById(R.id.btn_continuar).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText etAssinatura =  act.findViewById(R.id.et_nova_assinatura);
                    EditText etRepeticaoAssinatura =  act.findViewById(R.id.et_repita_assinatura);
                    
                    String assinatura = etAssinatura.getText().toString();
                    String repeticaoAssinatura = etRepeticaoAssinatura.getText().toString();
                    
                    CredentialsActivity act = (CredentialsActivity) getActivity();

                    if (!assinatura.equals(repeticaoAssinatura)) {
                        etRepeticaoAssinatura.setError(act.getString(R.string.erro_assinatura_nao_confere));
                        return;
                    }

                    if (assinatura.length() < Constantes.SIGNATURE_SIZE) {
                        etAssinatura.setError(act.getString(R.string.erro_assinatura_oito_digitos));
                        return;
                    }

                    UserPrefs.setSignature(act, repeticaoAssinatura);
                    act.nextStep();
                }
            });
        }
    }
}
