package br.com.meupag.investimentos.features.credentials.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.meupag.investimentos.R;

public class FirstStepFragment extends Fragment {

    private static final String TAG = FirstStepFragment.class.getSimpleName();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_first_step_credencials, null);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final CredentialsActivity act = (CredentialsActivity) getActivity();
        if(act != null){
            act.findViewById(R.id.btn_continuar).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    act.nextStep();
                }
            });
        }
    }
}
