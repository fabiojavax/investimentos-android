package br.com.meupag.investimentos.features.credentials.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import br.com.meupag.investimentos.BuildConfig;
import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.storage.UserPrefs;

public class FourthStepFragment extends Fragment {

    private static final String TAG = FourthStepFragment.class.getSimpleName();
    public static final String FRAGMENT_TAG = BuildConfig.APPLICATION_ID + "." + TAG;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_fourth_step_credencials, null);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final CredentialsActivity act = (CredentialsActivity) getActivity();
        if(act != null){
            act.findViewById(R.id.btn_continuar).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EditText etSenha =  act.findViewById(R.id.et_nova_senha);
                    EditText etRepeticaoSenha =  act.findViewById(R.id.et_repita_senha);

                    String senha = etSenha.getText().toString();
                    String repeticaoSenha = etRepeticaoSenha.getText().toString();

                    CredentialsActivity act = (CredentialsActivity) getActivity();

                    if (!senha.equals(repeticaoSenha)) {
                        etRepeticaoSenha.setError(act.getString(R.string.erro_senha_nao_confere));
                        return;
                    }

                    if (senha.length() < Constantes.PASSWORD_SIZE) {
                        etRepeticaoSenha.setError(act.getString(R.string.erro_senha_seis_digitos));
                        return;
                    }

                    UserPrefs.setPassword(act, repeticaoSenha);
                    act.nextStep();
                }
            });
        }
    }
}
