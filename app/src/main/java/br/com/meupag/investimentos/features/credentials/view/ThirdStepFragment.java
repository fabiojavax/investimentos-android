package br.com.meupag.investimentos.features.credentials.view;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;

import br.com.meupag.investimentos.BuildConfig;
import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.call.PostSendsms;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;
import br.com.meupag.investimentos.ws.transferobject.TokenTO;

public class ThirdStepFragment extends Fragment {

    private static final String TAG = ThirdStepFragment.class.getSimpleName();
    public static final String FRAGMENT_TAG = BuildConfig.APPLICATION_ID + "." + TAG;

    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_third_step_credencials, null);
        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final CredentialsActivity act = (CredentialsActivity) getActivity();
        if(act != null){
            final ProfileTO profileTO = UserPrefs.getProfile(act);

            TextView tvEmail = act.findViewById(R.id.tv_celular);
            tvEmail.setText(profileTO.phone);


            act.findViewById(R.id.btn_enviar_sms).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PostSendsms.RequestParams rp = new PostSendsms.RequestParams();
                    rp.customerId = profileTO.customerId;

                    progressDialog = new ProgressDialog(act);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setMessage(getString(R.string.msg_enviando_cod_sms));
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    PostSendsms.ResponseHandler rh = new PostSendsms.ResponseHandler() {
                        @Override
                        public void onSuccess(RetornoWS<TokenTO> retornoWS) {
                            progressDialog.dismiss();
                            openDialogToInsertPIN(act, retornoWS.getData().token);
                        }

                        @Override
                        public void onUnsuccess(RetornoWS<TokenTO> retornoWS) {
                            progressDialog.dismiss();
                            AlertAndroid.showMessageDialog(act, retornoWS.getMessage());
                        }

                        @Override
                        public void onError(IOException iOException) {
                            progressDialog.dismiss();
                            AlertAndroid.showMessageDialog(act, iOException);
                        }
                    };

                    new PostSendsms().setResponseHandler(rh).setRequestParams(rp).execute();
                }
            });
        }
    }

    public void openDialogToInsertPIN(final CredentialsActivity act, final String correctPIN){
        final View view = act.getLayoutInflater().inflate(R.layout.alert_token, null);
        final EditText pinCodeEditText = view.findViewById(R.id.et_token);

        final AlertDialog dialogPIN = new AlertDialog.Builder(act)
                .setTitle(R.string.confirmar_codigo)
                .setView(view)
                .setMessage(R.string.digite_o_codigo)
                .setNegativeButton(R.string.cancelar, null)
                .setPositiveButton(R.string.yes, null)
                .create();

        final View.OnClickListener onClickConfirmarInvestimento =
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String attempedCode = pinCodeEditText.getText().toString();

                        if(!attempedCode.equals(correctPIN)){
                            pinCodeEditText.setError(act.getString(R.string.erro_codigo_nao_confere));
                        }else{
                            act.nextStep();
                            dialogPIN.dismiss();
                        }
                    }
                };

        dialogPIN.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveButton = dialogPIN.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(null);
                positiveButton.setOnClickListener(onClickConfirmarInvestimento);
            }
        });

        dialogPIN.show();
    }
}
