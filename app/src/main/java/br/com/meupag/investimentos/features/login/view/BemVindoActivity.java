package br.com.meupag.investimentos.features.login.view;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.features.proposta.view.PropostaActivity;

public class BemVindoActivity extends AppCompatActivity {

    Activity activity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo_usuario);

        Button continuar = findViewById(R.id.button);

        continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, PropostaActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }
}
