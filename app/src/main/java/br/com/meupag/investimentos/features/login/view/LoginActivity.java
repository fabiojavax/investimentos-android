package br.com.meupag.investimentos.features.login.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import br.com.meupag.investimentos.BuildConfig;
import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.features.core.view.CoreActivity;
import br.com.meupag.investimentos.features.credentials.view.CredentialsActivity;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.util.ActivityUtils;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.CpfCnpjMasks;
import br.com.meupag.investimentos.util.Masks;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.util.StringHelper;
import br.com.meupag.investimentos.util.ValidaCPF;
import br.com.meupag.investimentos.ws.call.GetAccountRecover;
import br.com.meupag.investimentos.ws.call.GetCustomerAccount;
import br.com.meupag.investimentos.ws.call.GetProductContact;
import br.com.meupag.investimentos.ws.call.PostAccountLogin;
import br.com.meupag.investimentos.ws.transferobject.AccountTO;
import br.com.meupag.investimentos.ws.transferobject.LoginTO;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;
import com.amazonaws.util.StringUtils;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    Activity activity = this;

    ProgressBar progressBar;
    EditText editTextCPF;
    EditText editTextPass;
    Button buttonEntrar;
    ProgressDialog progressDialog;
    TextView esqueciSenhaTextView;

    AccountTO accountTO;

    String phoneNumber;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);

        progressBar             = findViewById(R.id.progress_bar);
        editTextPass            = findViewById(R.id.edit_text_pass);
        editTextCPF             = findViewById(R.id.edit_text_login);
        buttonEntrar            = findViewById(R.id.button_entrar);
        esqueciSenhaTextView    = findViewById(R.id.tv_esqueci_senha);

        editTextCPF.addTextChangedListener(CpfCnpjMasks.insert(editTextCPF));
        editTextCPF.addTextChangedListener(changeCpfWatcher);
        editTextCPF.addTextChangedListener(validadorDeCPF);

        esqueciSenhaTextView.setOnClickListener(onClickEsqueciSenha);


//        if (BuildConfig.DEBUG) {
//            editTextCPF.setText(Constantes.CPF_FABIO);
//        }

        String lastUsedCPF = UserPrefs.getPrefLastCustomerId(activity);
        if(StringHelper.isNotBlank(lastUsedCPF)){
            editTextCPF.setText(lastUsedCPF);
        }

        buttonEntrar.setOnClickListener(onClickBotaoEntrar);
        editTextPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextPass.setError(null);
            }
        });

        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);

    }

    final private GetCustomerAccount.ResponseHandler customerAccountResponseHandler = new GetCustomerAccount.ResponseHandler() {
        @Override
        public void onSuccess(AccountTO data) {
            progressDialog.dismiss();
            accountTO = data;
            /*  if nao tem conta
                    if tem proposta em aprovacao? joga pra tela de aguarde, proposta esta em aprovacao
                    if nao tem proposta ou proposta incompleta? joga pra proposta
                if tem conta
                    if nao tem credentials? criar credentials -> jogar pro app
                    if tem credentials? joga pra tela de logar com a senha
            */

            if(!data.account){
                ProfileTO profileTO = new ProfileTO();
                profileTO.customerId = data.customerId;
                UserPrefs.setProfile(activity, profileTO);
                accountTO = null;
                if(data.proposal && data.waiting){
                    Intent intent = new Intent(activity, AguardandoActivity.class);
                    activity.startActivity(intent);
                } else{
                    Intent intent = new Intent(activity, BemVindoActivity.class);
                    activity.startActivity(intent);
                }
            }else{
                if(data.credentials) {
                    animEditTextPass(true);
                    editTextPass.requestFocus();
                }else {
                    accountTO = null;
                    ProfileTO profileTO = new ProfileTO();
                    profileTO.email = data.email;
                    profileTO.phone = data.phone;
                    profileTO.customerId = data.customerId;
                    UserPrefs.setProfile(activity, profileTO);
                    activity.startActivity(new Intent(activity, CredentialsActivity.class));
                }

            }

        }

        @Override
        public void onUnsuccess(RetornoWS<AccountTO> retornoWS) {
            progressDialog.dismiss();
            AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
        }

        @Override
        public void onError(IOException iOException) {
            progressDialog.dismiss();
            AlertAndroid.showMessageDialog(activity, iOException);
        }
    };

    final private PostAccountLogin.ResponseHandler postLoginResponseHandler = new PostAccountLogin.ResponseHandler() {
        @Override
        public void onSuccess(LoginTO data) {
            progressDialog.dismiss();
            ProfileTO profileTO = new ProfileTO();
            profileTO.customerId = data.customerId;
            profileTO.signature = data.signature;
            UserPrefs.setProfile(activity, profileTO);
            UserPrefs.setSignature(activity, data.signature);
            UserPrefs.setAccountNumber(activity, data.accountNumber);
            UserPrefs.setLastUsedCustomerId(activity, editTextCPF.getText().toString());

            finish();
            activity.startActivity(new Intent(activity, CoreActivity.class));
        }

        @Override
        public void onUnsuccess(RetornoWS<LoginTO> retornoWS) {
            progressDialog.dismiss();
            AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
        }

        @Override
        public void onError(IOException iOException) {
            progressDialog.dismiss();
            AlertAndroid.showMessageDialog(activity, iOException);
        }
    };


    View.OnClickListener onClickBotaoEntrar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            String cpf = CpfCnpjMasks.unmask(editTextCPF);
            String pass = editTextPass.getText().toString();


            if(accountTO == null || !accountTO.customerId.equals(cpf)){

                if(!ValidaCPF.isCPF(cpf)){
                    editTextCPF.setError(getString(R.string.erro_cpf_invalido));
                    return;
                }

                GetCustomerAccount.RequestParams params = new GetCustomerAccount.RequestParams();
                params.customerId = cpf;
                progressDialog.setMessage(getString(R.string.msg_aguarde));
                progressDialog.show();
                GetCustomerAccount.doCall(activity, params, customerAccountResponseHandler);
            } else {
                if (StringUtils.isBlank(pass)) {
                    AlertAndroid.showMessageDialog(activity, getString(R.string.informe_senha));
                } else {
                    PostAccountLogin.RequestParams params = new PostAccountLogin.RequestParams();
                    params.customerId = cpf;
                    params.password = pass;
                    progressDialog.setMessage(getString(R.string.msg_acessando_conta));
                    progressDialog.show();
                    ActivityUtils.hideKeyboard(activity);
                    PostAccountLogin.doCall(activity, params, postLoginResponseHandler);
                }
            }
        }
    };

    View.OnClickListener onClickEsqueciSenha = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            DialogInterface.OnClickListener enviarSenhaListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    doGetAccountRecover();
                }
            };

            DialogInterface.OnClickListener entrarEmContatoListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    openDialogToInsertContato();
                }
            };

            DialogInterface.OnClickListener dismissListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };

            AlertDialog.Builder confirmDialogBuilder = new AlertDialog.Builder(activity);

            confirmDialogBuilder
                    .setCancelable(false)
                    .setTitle(R.string.recuperar_senha)
                    .setMessage(getString(R.string.msg_enviar_senha_email) + accountTO.email)
                    .setPositiveButton(R.string.enviar, enviarSenhaListener)
                    .setNegativeButton(R.string.contato, entrarEmContatoListener)
                    .setNeutralButton(R.string.cancelar, dismissListener);

            AlertDialog alert = confirmDialogBuilder.create();
            alert.show();
        }
    };

    public void openDialogToInsertContato() {
        final View view = activity.getLayoutInflater().inflate(R.layout.alert_contact, null);
        final EditText contactEditText = view.findViewById(R.id.et_contact);

        Masks.setCelularMask(contactEditText);

        final AlertDialog dialogContact = new AlertDialog.Builder(activity)
                .setTitle(R.string.recuperar_senha)
                .setView(view)
                .setMessage(R.string.digite_telefone)
                .setNegativeButton(R.string.cancelar, null)
                .setPositiveButton(R.string.enviar, null)
                .create();

        final View.OnClickListener onClickEnviarContato =
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        phoneNumber = contactEditText.getText().toString();

                        if (StringHelper.isBlank(phoneNumber)) {
                            contactEditText.setError(activity.getString(R.string.erro_numero_telefone_vazio));
                        }else{
                            dialogContact.dismiss();
                            doGetProductContact();
                        }
                    }
                };

        dialogContact.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveButton = dialogContact.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(null);
                positiveButton.setOnClickListener(onClickEnviarContato);
            }
        });

        dialogContact.show();
    }

    public void doGetProductContact() {
        progressDialog.setMessage(getString(R.string.msg_enviando_contato));
        progressDialog.show();

        GetProductContact.ResponseHandler rh = new GetProductContact.ResponseHandler() {
            @Override
            public void onSuccess(Object data) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, getString(R.string.msg_contato_enviado));
            }

            @Override
            public void onUnsuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
            }

            @Override
            public void onError(IOException iOException) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, iOException);
            }
        };

        GetProductContact.RequestParams rp = new GetProductContact.RequestParams();
        rp.customerId = accountTO.customerId;

        new GetProductContact().setRensponseHandler(rh).setRequestParams(rp).execute();
    }

    public void doGetAccountRecover() {
        progressDialog.setMessage(getString(R.string.msg_enviando_email));
        progressDialog.show();
        GetAccountRecover.ResponseHandler rh = new GetAccountRecover.ResponseHandler() {
            @Override
            public void onSuccess(Object data) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, getString(R.string.msg_senha_enviada_email));
            }

            @Override
            public void onUnsuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
            }

            @Override
            public void onError(IOException iOException) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, iOException);
            }
        };

        GetAccountRecover.RequestParams rp = new GetAccountRecover.RequestParams();
        rp.customerId = accountTO.customerId;

        new GetAccountRecover().setRensponseHandler(rh).setRequestParams(rp).execute();
    }

    TextWatcher changeCpfWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String cpf = CpfCnpjMasks.unmask(s.toString());
            if(accountTO != null && !cpf.equals(accountTO.customerId)){

                animEditTextPass(false);

                editTextPass.setText("");
                accountTO = null;
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void animEditTextPass(boolean up) {
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) buttonEntrar.getLayoutParams();

        int delta = buttonEntrar.getMeasuredHeight() + lp.topMargin;
        float alpha;

//        if (BuildConfig.DEBUG) {
//            editTextPass.setText(up ? "111111" : "");
//        }

        View toHide = activity.findViewById(R.id.edit_text_pass_layout);

        if(up){
            delta = delta * -1;
            alpha = 1.0f;
            toHide.setEnabled(true);
        }else{
            alpha = 0.0f;
            toHide.setEnabled(false);
        }


        toHide.animate()
            .yBy(delta)
            .alpha(alpha)
            .setInterpolator(new AccelerateDecelerateInterpolator())
            .setDuration(300);

        esqueciSenhaTextView.animate()
                .alpha(alpha)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(300);

        View loginLayout = activity.findViewById(R.id.edit_text_login_layout);
        loginLayout.animate().yBy(delta)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(300);

        View logo = activity.findViewById(R.id.logo);
        logo.animate().yBy(delta)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .setDuration(300);
    }

    TextWatcher validadorDeCPF = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

            String cpf = CpfCnpjMasks.unmask(editable.toString());

            if(cpf.length() == 11 && !ValidaCPF.isCPF(cpf)){
                editTextCPF.setError(getString(R.string.erro_cpf_invalido));
                buttonEntrar.setEnabled(false);
            }else{
                editTextCPF.setError(null);
                buttonEntrar.setEnabled(true);
            }

        }
    };

}