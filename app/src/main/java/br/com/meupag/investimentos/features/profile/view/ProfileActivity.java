package br.com.meupag.investimentos.features.profile.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.databinding.ActivityProfileBinding;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.storage.db.AppDatabase;
import br.com.meupag.investimentos.storage.db.DomainDAO;
import br.com.meupag.investimentos.storage.model.Domain;
import br.com.meupag.investimentos.util.ActivityUtils;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.FmtUtils;
import br.com.meupag.investimentos.util.Masks;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.call.PostProfile;
import br.com.meupag.investimentos.ws.call.PostWithdraw;
import br.com.meupag.investimentos.ws.call.PutCredentials;
import br.com.meupag.investimentos.ws.transferobject.LoginTO;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;

public class ProfileActivity extends AppCompatActivity {

    public static String TAG = ProfileActivity.class.getSimpleName();
    public static String CARDVIEW_TO_SHOW = "cardview_to_show";
    public static String SALDO_PARAM = "saldo_param";

    Activity activity = this;
    int cardViewToShow = 0;
    ActivityProfileBinding b;

    AppDatabase appDb;
    DomainDAO domainDAO;

    Double saldo;

    AlertDialog dialogAssinatura;
    ProgressDialog progressDialog;
    boolean dialogAssinaturaReady = false;

    ActionBar actionBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = DataBindingUtil.setContentView(this, R.layout.activity_profile);

        Intent intent = getIntent();
        cardViewToShow = intent.getIntExtra(CARDVIEW_TO_SHOW, 0);
        saldo = intent.getDoubleExtra(SALDO_PARAM, 0.0);
        b.tvValorRetirada.setText(FmtUtils.realValue(saldo));

        prepareDAOs();
        setUpToolBar(cardViewToShow);
        setUpVisibleCardview(cardViewToShow);

        ProfileTO profileTO = UserPrefs.getProfile(this);
        String [] nomeCompleto = profileTO.name.split("\\s");
        b.etNome.setText(nomeCompleto[0]);
        b.etSobrenome.setText(nomeCompleto[nomeCompleto.length-1]);

        String nomeDoMeio = "";
        String delimiter = "";
        for (int i = 1; i < nomeCompleto.length-1; i++) {
            nomeDoMeio = nomeDoMeio + delimiter + nomeCompleto[i];
            delimiter = " ";
        }

        b.etNomeMeio.setText(nomeDoMeio);
        Masks.setCelularMask(b.etTelefone);
        b.etTelefone.setText(profileTO.phone);
        b.etEmail.setText(profileTO.email);

        setUpRealizarTED();
        b.botaoRetirar.setOnClickListener(onClickBotaoRetirar);

        new ShowDomainInfo().execute(profileTO);
    }

    private void setUpVisibleCardview(int cardViewToShow) {
        b.llDadosBancarios.setVisibility(View.INVISIBLE);
        b.llNomeEmailTel.setVisibility(View.INVISIBLE);
        b.llSenha.setVisibility(View.INVISIBLE);
        b.llSolicitarTed.setVisibility(View.INVISIBLE);

        if(cardViewToShow != 0){
            b.baseView.findViewById(cardViewToShow).setVisibility(View.VISIBLE);
        }
    }

    private void setUpToolBar(int visibleCardView) {
        setSupportActionBar(b.toolbar);
        actionBar = getSupportActionBar();

        if(actionBar != null){
            actionBar.setDisplayHomeAsUpEnabled(true);

            switch (visibleCardView){
                case R.id.ll_senha:
                    actionBar.setTitle(R.string.senha);
                    break;
                case R.id.ll_dados_bancarios:
                    actionBar.setTitle(R.string.banco);
                    break;
                case R.id.ll_solicitar_ted:
                    actionBar.setTitle(R.string.ted);
                    showSaldo(saldo);
                    break;
            }
        }
    }

    private void showSaldo(double saldo){
        String saldoValor = getString(R.string.saldo_valor);
        saldoValor += FmtUtils.realValue(saldo);
        actionBar.setSubtitle(saldoValor);
    }

    private void prepareDAOs() {
        appDb = Room.databaseBuilder(this, AppDatabase.class, "db-invista").build();
        domainDAO = appDb.getDomainDAO();
    }

    @Override
    public boolean onSupportNavigateUp() {
        ActivityUtils.hideKeyboard(activity);
        finish();
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(cardViewToShow == R.id.ll_nome_email_tel
                || cardViewToShow == R.id.ll_senha
                || cardViewToShow == R.id.ll_assinatura){
            getMenuInflater().inflate(R.menu.menu_profile, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.action_save){
            switch (cardViewToShow){
                case R.id.ll_nome_email_tel:
                    saveProfile();
                    break;
                case R.id.ll_senha:
                    alterPasswordAndSignature();
                    break;
            }
            return true;
        }
        return false;
    }

    private class ShowDomainInfo extends AsyncTask<ProfileTO, Void, Boolean>{
        @Override
        protected Boolean doInBackground(ProfileTO... profileTOS) {
            final ProfileTO prof = profileTOS[0];

            if(prof == null){
                return false;
            }

            Domain domain = domainDAO.getDomainByTypeWithCodigo(Domain.BANK_TYPE, prof.bankInfo.bankCode);

            final String bankName = domain == null ? prof.bankInfo.bankCode : domain.getDescription();

            domain = domainDAO.getDomainByTypeWithCodigo(Domain.ACCOUNT_TYPE, prof.bankInfo.accountType);

            final String accountName = domain == null ? prof.bankInfo.accountType : domain.getDescription();

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    b.etBanco.setText(bankName);
                    b.etAgencia.setText(prof.bankInfo.bankAgency);
                    b.etTipoConta.setText(accountName);
                    b.etNumeroConta.setText(prof.bankInfo.accountNumber);


                    b.etBancoTed.setText(bankName);
                    b.etAgenciaTed.setText(prof.bankInfo.bankAgency);
                    b.etTipoContaTed.setText(accountName);
                    b.etNumeroContaTed.setText(prof.bankInfo.accountNumber);
                }
            });

            return true;
        }
    }

    private void saveProfile() {

        final ProfileTO profileTO = UserPrefs.getProfile(this);
        profileTO.phone = Masks.unmask(b.etTelefone.getText().toString());
        profileTO.name = b.etNome.getText().toString().trim()+ " "
                + b.etNomeMeio.getText().toString().trim()+ " "
                + b.etSobrenome.getText().toString().trim();
        profileTO.email = b.etEmail.getText().toString();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.msg_enviando_alteracao_perfil));
        progressDialog.setCancelable(false);

        final AlertDialog sucessDialog = new AlertDialog.Builder(activity)
                .setMessage(getString(R.string.perfil_alterado_sucesso))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setResult(Activity.RESULT_OK);
                        finish();
                    }
                }).create();

        AlertDialog.Builder adb = new AlertDialog.Builder(activity);
        adb.setMessage(R.string.save_profile_confirmation);
        adb.setIcon(android.R.drawable.ic_dialog_alert);
        adb.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ActivityUtils.hideKeyboard(activity);
                progressDialog.show();
                new PostProfile().setRensponseHandler(new PostProfile.RensponseHandler() {
                    @Override
                    public void onSuccess() {
                        UserPrefs.setProfile(activity, profileTO);
                        progressDialog.dismiss();
                        sucessDialog.show();
                    }

                    @Override
                    public void onError(RetornoWS<Object> retornoWS) {
                        progressDialog.dismiss();
                        AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
                    }
                }).execute(profileTO);
            }
        });
        adb.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            } });
        adb.show();

    }

    private void alterPasswordAndSignature(){
        ProfileTO profileTO = UserPrefs.getProfile(activity);

        String currentPassword = b.etSenhaAtual.getText().toString();
        String newPassword = b.etNovaSenha.getText().toString();
        String newPasswordConfirmation = b.etRepitaSenha.getText().toString();

        String currentSignature = b.etAssinaturaAtual.getText().toString();
        final String newSignature = b.etNovaAssinatura.getText().toString();
        String newSignatureConfirmation = b.etRepitaAssinatura.getText().toString();

        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.msg_enviando_alteracao_senha));
        progressDialog.setCancelable(false);

        final AlertDialog alteracaoSenhaSuccessDialog = new AlertDialog.Builder(activity)
                .setMessage(getString(R.string.msg_credenciais_alteradas))
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityUtils.hideKeyboard(activity);
                        finish();
                    }
                }).create();

        String realCurrentSignature = UserPrefs.getSignature(activity);

        if(!currentSignature.equals(realCurrentSignature)){
            b.etAssinaturaAtual.setError(getString(R.string.erro_assinatura_atual_nao_confere));
            b.etAssinaturaAtual.requestFocus();
            return;
        }

        if(!newPassword.equals(newPasswordConfirmation)){
            b.etRepitaSenha.setError(getString(R.string.erro_senha_nao_confere));
            b.etRepitaSenha.requestFocus();
        } else if(!newSignature.equals(newSignatureConfirmation)){
            b.etRepitaAssinatura.setError(getString(R.string.erro_assinatura_nao_confere));
            b.etRepitaAssinatura.requestFocus();
        } else {
            progressDialog.show();
            new PutCredentials().addField(PutCredentials.CUSTOMER_ID, profileTO.customerId)
                    .addField(PutCredentials.PASSWORD, currentPassword)
                    .addField(PutCredentials.NEW_PASSWORD, newPassword)
                    .addField(PutCredentials.SIGNATURE, currentSignature)
                    .addField(PutCredentials.NEW_SIGNATURE, newSignature)
                    .setRensponseHandler(new PutCredentials.RensponseHandler() {
                        @Override
                        public void onSuccess() {
                            progressDialog.dismiss();
                            alteracaoSenhaSuccessDialog.show();
                            UserPrefs.setSignature(activity, newSignature);
                        }

                        @Override
                        public void onError(RetornoWS<LoginTO> retornoWS) {
                            progressDialog.dismiss();
                            b.etNovaAssinatura.setError(retornoWS.getMessage());
                            b.etNovaAssinatura.requestFocus();
                        }
                    }).execute();
        }

    }

    private void doPostWithDraw(){
        final ProfileTO profileTO = UserPrefs.getProfile(activity);

        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.msg_enviando_solicitcao_ted));
        progressDialog.setCancelable(false);


        progressDialog.show();
        PostWithdraw post = new PostWithdraw().setRensponseHandler(
                new PostWithdraw.RensponseHandler() {
                    @Override
                    public void onSuccess(RetornoWS<Object> retornoWS) {
                        progressDialog.dismiss();
                        String successMsg = getString(R.string.msg_ted_sucesso, FmtUtils.realValue(saldo));
                        AlertAndroid.showConfirmDialog(activity, successMsg, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                showSaldo(0.0);
                                setResult(Activity.RESULT_OK);
                                finish();
                            }
                        });
                    }

                    @Override
                    public void onUnsuccess(RetornoWS<Object> retornoWS) {
                        progressDialog.dismiss();
                        AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
                    }

                    @Override
                    public void onError(IOException iOException) {
                        progressDialog.dismiss();
                        AlertAndroid.showMessageDialog(activity, iOException);
                    }
                });

        post.requestParams.customerId = profileTO.customerId;
        post.requestParams.amount = saldo;
        post.requestParams.accountNumber = Integer.valueOf(profileTO.bankInfo.accountNumber);
        post.execute();

    }

    private View.OnClickListener onClickBotaoRetirar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dialogAssinatura.show();
        }
    };

    private void setUpRealizarTED() {
        final View view = activity.getLayoutInflater().inflate(R.layout.alert_assinatura, null);
        final EditText assinaturaEditText = view.findViewById(R.id.et_assinatura);
        final View.OnClickListener onClickOK =
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String correctSignature = UserPrefs.getSignature(activity);//pode ser null
                        String attempedSignature = assinaturaEditText.getText().toString();

                        if(!attempedSignature.equals(correctSignature)){
                            assinaturaEditText.setError(getString(R.string.erro_assinatura_nao_confere));
                        }else{
                            doPostWithDraw();
                            dialogAssinatura.dismiss();
                        }
                    }
                };

        dialogAssinatura = new AlertDialog.Builder(activity)
                .setTitle(R.string.ass_eletronica)
                .setView(view)
                .setMessage(R.string.informe_ass_eletronica)
                .setNegativeButton(R.string.cancelar, null)
                .setPositiveButton(R.string.yes, null)
                .create();

        dialogAssinatura.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                if (!dialogAssinaturaReady) {
                    dialogAssinatura.getButton(DialogInterface.BUTTON_POSITIVE)
                            .setOnClickListener(onClickOK);
                    dialogAssinaturaReady = true;
                }
            }
        });
    }

}
