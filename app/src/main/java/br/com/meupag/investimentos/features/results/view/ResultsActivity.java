package br.com.meupag.investimentos.features.results.view;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.Date;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.databinding.ActivityResultsBinding;
import br.com.meupag.investimentos.features.core.view.CoreActivity;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.util.ActivityUtils;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.FmtUtils;
import br.com.meupag.investimentos.util.InvistaUtils;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.call.GetStockTax;
import br.com.meupag.investimentos.ws.call.PostStockDelete;
import br.com.meupag.investimentos.ws.call.PostStockSell;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;
import br.com.meupag.investimentos.ws.transferobject.TaxTO;
import br.com.meupag.investimentos.ws.transferobject.WalletTO;

public class ResultsActivity extends AppCompatActivity {

    Activity activity = this;

    ActivityResultsBinding b;

    WalletTO.TituloTO tit;
    LinearLayout llBottomSheet;
    ProgressDialog progressDialog;
    AlertDialog dialogCriticaResgate;
    BottomSheetBehavior bottomSheetBehavior;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = DataBindingUtil.setContentView(this, R.layout.activity_results);

        llBottomSheet       = findViewById(R.id.bottom_sheet_detail);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);

        tit = getIntent().getParcelableExtra(WalletTO.TituloTO.NAME);

        b.ganho.setText(FmtUtils.percent(tit.taxa));
        b.iof.setText(FmtUtils.realValue(tit.valorIOF));
        b.remuneracao.setText(InvistaUtils.taxa(tit.taxa));
        b.tvTipoTaxa.setText(InvistaUtils.tipoTaxa(tit.taxa));
        b.lucro.setText(FmtUtils.realValue(tit.valorRendimento));
        b.impostoRenda.setText(FmtUtils.realValue(tit.valorImposto));
        b.dataAplicacao.setText(FmtUtils.dateUStoBR(tit.dataEmissao));
        b.vencimento.setText(FmtUtils.dateUStoBR(tit.dataVencimento));
        b.valorBruto.setText(FmtUtils.realValue(tit.valorResgateBruto));
        b.valorAplicado.setText(FmtUtils.realValue(tit.valorPrincipal));
        b.valorLiquido.setText(FmtUtils.realValue(tit.valorResgateLiquido));

        b.setTitulo(tit);

        configToolbar();

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);


        b.rowVencimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDetailOf(R.id.row_vencimento);
            }
        });

        b.btnAnteciparResgate.setOnClickListener(onClickAnteciparResgate);
        dialogCriticaResgate = configDialogCriticaResgate();

    }

    private void configToolbar() {
        setSupportActionBar(b.toolbar);
        ActionBar ab = getSupportActionBar();
        if(ab != null){
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private AlertDialog configDialogCriticaResgate() {
        return new AlertDialog.Builder(activity)
                .setPositiveButton(R.string.continuar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        doSellOrDelete();
                    }
                })
                .setNegativeButton(R.string.cancelar, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).create();
    }

    public void doSellOrDelete() {

        String currenteDate = FmtUtils.YYYY_MM_DD.format(new Date());

        boolean canDelete = currenteDate.equals(tit.dataEmissao);


        if(canDelete){
            ActivityUtils.configSignatureDialogToAction(activity, null, new ActivityUtils.ActionExecutor() {
                @Override
                public void execute() {
                    doPostStockDelete();
                }
            });
        }else{
            ActivityUtils.configSignatureDialogToAction(activity, null, new ActivityUtils.ActionExecutor() {
                @Override
                public void execute() {
                    doPostStockSell();
                }
            });
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }



    private void showDetailOf(int id){
        int msgId;
        int titleId;
        switch (id){
            case R.id.row_vencimento:
                msgId = R.string.detalhe_vencimento;
                titleId = R.string.vencimento;
                break;
            default:
                msgId=0;
                titleId=0;
        }

        if(msgId != 0){
            ((TextView)llBottomSheet.findViewById(R.id.tv_detail)).setText(msgId);
            ((TextView)llBottomSheet.findViewById(R.id.tv_detail_title)).setText(titleId);
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }


    View.OnClickListener onClickAnteciparResgate = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progressDialog = new ProgressDialog(activity);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressDialog.setMessage(getString(R.string.msg_enviando_solicitcao_resgate));
            progressDialog.setCancelable(false);
            progressDialog.show();

            GetStockTax.ResponseHandler rh = new GetStockTax.ResponseHandler() {
                @Override
                public void onSuccess(TaxTO data) {
                    progressDialog.dismiss();

                    Resources res = getResources();
                    String taxType = TaxTO.IOF.equals(data.type) ? res.getString(R.string.iof)
                            : res.getString(R.string.irrf);
                    String taxValue = FmtUtils.percent(data.currentDiscount);

                    String critica =  res.getQuantityString(R.plurals.critica_resgate,
                            data.remainingDays, taxType, taxValue, data.remainingDays);
                    dialogCriticaResgate.setMessage(critica);
                    dialogCriticaResgate.show();
                }

                @Override
                public void onUnsuccess(RetornoWS<TaxTO> retornoWS) {
                    progressDialog.dismiss();
                    AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
                }

                @Override
                public void onError(IOException iOException) {
                    progressDialog.dismiss();
                    AlertAndroid.showMessageDialog(activity, iOException);
                }
            };

            ProfileTO profileTO = UserPrefs.getProfile(activity);

            GetStockTax.RequestParams requestParams = new GetStockTax.RequestParams();
            requestParams.customerId = profileTO.customerId;
            requestParams.stockId = String.valueOf(tit.numeroTitulo);
            requestParams.buyDate = tit.dataEmissao;

            GetStockTax getStockTax = new GetStockTax();
            getStockTax.setRequestParams(requestParams);
            getStockTax.setRensponseHandler(rh);
            getStockTax.execute();

        }
    };

    private void doPostStockDelete(){
        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.msg_enviando_solicitcao_resgate));
        progressDialog.setCancelable(false);
        progressDialog.show();

        PostStockDelete.ResponseHandler rh = new PostStockDelete.ResponseHandler() {
            @Override
            public void onSuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showConfirmDialog(activity, R.string.successo_resgate, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent returnToCoreActivity = new Intent(activity, CoreActivity.class);
                        returnToCoreActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(returnToCoreActivity);
                        finish();
                    }
                });
            }

            @Override
            public void onUnsuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
            }

            @Override
            public void onError(IOException iOException) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, iOException);
            }
        };

        ProfileTO profileTO = UserPrefs.getProfile(activity);

        PostStockDelete postStockDelete = new PostStockDelete();
        postStockDelete.setResponseHandler(rh);
        postStockDelete.requestParams.customerId = profileTO.customerId;
        postStockDelete.requestParams.stockId = tit.numeroTitulo;
        postStockDelete.execute();
    }

    private void doPostStockSell(){
        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.msg_enviando_solicitcao_resgate));
        progressDialog.setCancelable(false);
        progressDialog.show();

        PostStockSell.ResponseHandler rh = new PostStockSell.ResponseHandler() {
            @Override
            public void onSuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showConfirmDialog(activity, R.string.successo_resgate, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent returnToCoreActivity = new Intent(activity, CoreActivity.class);
                        returnToCoreActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(returnToCoreActivity);
                        finish();
                    }
                });
            }

            @Override
            public void onUnsuccess(RetornoWS<Object> retornoWS) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
            }

            @Override
            public void onError(IOException iOException) {
                progressDialog.dismiss();
                AlertAndroid.showMessageDialog(activity, iOException);
            }
        };

        ProfileTO profileTO = UserPrefs.getProfile(activity);

        PostStockSell postStockSell = new PostStockSell();
        postStockSell.setResponseHandler(rh);
        postStockSell.requestParams.customerId = profileTO.customerId;
        postStockSell.requestParams.stockId = tit.numeroTitulo;
        postStockSell.execute();
    }

}
