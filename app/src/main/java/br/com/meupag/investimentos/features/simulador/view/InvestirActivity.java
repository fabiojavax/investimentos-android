package br.com.meupag.investimentos.features.simulador.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.Date;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.features.core.view.CoreActivity;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.util.ActivityUtils;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.FmtUtils;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.call.PostStockBuy;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;

import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.DATA_VENCIMENTO;
import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.NOME_APLICACAO;
import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.PERIOD;
import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.REMUNERACAO;
import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.SALDO;
import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.SAVINGS_PERCENTAGE;
import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.STOCK_TYPE;
import static br.com.meupag.investimentos.features.simulador.view.SimuladorActivity.VALOR_APLICADO;

public class InvestirActivity extends AppCompatActivity {

    Activity activity = this;

    Button          btnInvestir;
    ProgressDialog  progressDialog;

    float valorAplicado;

    int period;
    int stockType;

    String saldo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investir);

        btnInvestir                     = findViewById(R.id.botao_investir);

        TextView ganho                  = findViewById(R.id.ganho);
        TextView nomeAplTextView        = findViewById(R.id.nome_aplicacao);
        TextView dataVencimentoTextView = findViewById(R.id.data_vencimento);
        TextView valorAplicadoTextView  = findViewById(R.id.valor_aplicado);
        TextView dataAplicacaoTextView  = findViewById(R.id.data_aplicacao);
        TextView remuneracaoTextView    = findViewById(R.id.remuneracao);

        Intent intent = getIntent();

        saldo                   = intent.getStringExtra(SALDO);
        period                  = intent.getIntExtra(PERIOD, 0);
        stockType               = intent.getIntExtra(STOCK_TYPE, 0);
        valorAplicado           = intent.getFloatExtra(VALOR_APLICADO, 0);

        String remuneracao      = intent.getStringExtra(REMUNERACAO);
        String nomeAplicacao    = intent.getStringExtra(NOME_APLICACAO);
        String dataVencimento   = intent.getStringExtra(DATA_VENCIMENTO);
        float savingsPercentage = intent.getFloatExtra(SAVINGS_PERCENTAGE,0);

        configToolbar();

        nomeAplTextView.setText(nomeAplicacao);
        remuneracaoTextView.setText(remuneracao);
        ganho.setText(FmtUtils.percent(savingsPercentage));
        dataVencimentoTextView.setText(FmtUtils.dateUStoBR(dataVencimento));
        dataAplicacaoTextView.setText(FmtUtils.DD_MM_YYYY.format(new Date()));
        valorAplicadoTextView.setText(FmtUtils.realValue((double) valorAplicado));

        ActivityUtils.configSignatureDialogToAction(
                activity,
                btnInvestir,
                new ActivityUtils.ActionExecutor() {
                    @Override
                    public void execute() {
                        doPostStockBuy();
                    }
                });
    }

    private void configToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setSubtitle(getString(R.string.novo_saldo_valor) + saldo);
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void doPostStockBuy(){
        progressDialog = new ProgressDialog(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage(getString(R.string.msg_enviando_solicitcao_compra));
        progressDialog.setCancelable(false);
        progressDialog.show();


        ProfileTO profileTO = UserPrefs.getProfile(activity);

        PostStockBuy post = new PostStockBuy().setRensponseHandler(
                new PostStockBuy.RensponseHandler() {
                    @Override
                    public void onSuccess(RetornoWS<Object> retornoWS) {
                        progressDialog.dismiss();
                        AlertAndroid.showConfirmDialog(activity, R.string.sucesso_compra, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                Intent returnToCoreActivity = new Intent(activity, CoreActivity.class);
                                returnToCoreActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(returnToCoreActivity);
                                finish();
                            }
                        });
                    }

                    @Override
                    public void onUnsuccess(RetornoWS<Object> retornoWS) {
                        progressDialog.dismiss();
                        AlertAndroid.showMessageDialog(activity, retornoWS.getMessage());
                    }

                    @Override
                    public void onError(IOException iOException) {
                        progressDialog.dismiss();
                        AlertAndroid.showMessageDialog(activity, iOException);
                    }
                });

        post.requestParams.amount           = FmtUtils.toBigDecimal(valorAplicado);
        post.requestParams.period           = period;
        post.requestParams.stockType        = stockType;
        post.requestParams.customerId       = profileTO.customerId;
        post.requestParams.accountNumber    = UserPrefs.getPrefAccountNumberCfi(activity);
        post.execute();
    }


}
