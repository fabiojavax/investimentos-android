package br.com.meupag.investimentos.features.simulador.view;

import android.animation.Animator;
import android.app.Activity;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.SeekBar;
import android.widget.TextView;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.databinding.ActivitySimuladorBinding;
import br.com.meupag.investimentos.features.simulador.viewmodel.SimuladorViewModel;
import br.com.meupag.investimentos.storage.UserPrefs;
import br.com.meupag.investimentos.util.ActivityUtils;
import br.com.meupag.investimentos.util.AlertAndroid;
import br.com.meupag.investimentos.util.FmtUtils;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.widget.MoneyTextWatcher;
import br.com.meupag.investimentos.ws.call.GetStockRates;
import br.com.meupag.investimentos.ws.transferobject.StockRatesTO;

import static br.com.meupag.investimentos.ws.transferobject.StockRatesTO.POS_FIXADO;

public class SimuladorActivity extends AppCompatActivity {

    public static final String TAG                 = SimuladorActivity.class.getSimpleName();
    public static final String REMUNERACAO         = "remuneracao";
    public static final String NOME_APLICACAO      = "nome_aplicacao";
    public static final String VALOR_APLICADO      = "valor_aplicado";
    public static final String DATA_VENCIMENTO     = "data_vencimento";
    public static final String STOCK_TYPE          = "stockType";
    public static final String PERIOD              = "periodo";
    public static final String SALDO               = "saldo";
    public static final String SAVINGS_PERCENTAGE  = "savingsPercentage";

    private static final double VALOR_INICIAL       = 0;
    public static final int     DURATION_MILLIS     = 300;

    Activity activity = this;

    int[] COLORS;
    Animations anim;
    ActivitySimuladorBinding b;
    int stockType = POS_FIXADO;
    SimuladorViewModel viewModel;
    StockRatesTO.RateTO selectedRate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        b = DataBindingUtil.setContentView(this, R.layout.activity_simulador);
        viewModel = ViewModelProviders.of(this).get(SimuladorViewModel.class);

        stockType = getIntent().getIntExtra(STOCK_TYPE, stockType);

        configToolbar();

        COLORS = new int[]{ColorTemplate.colorWithAlpha(ColorTemplate.rgb("118AB2"), 225),
                ColorTemplate.colorWithAlpha(ColorTemplate.rgb("33658A"), 225)};

        loadData();

        viewModel.rates.observe(this, new Observer<List<StockRatesTO.RateTO>>() {
            @Override
            public void onChanged(@Nullable List<StockRatesTO.RateTO> integerRateTOMap) {
                selectedRate = viewModel.getMinRate();
                setupChart();
                setupSeekBar();
            }
        });


        anim = new Animations();
        anim.first();
        b.continuar.setOnClickListener(onClickContinuar);
        b.valorInicial.setOnEditorActionListener(valorInicialActionListener);
        b.valorInicial.setText(R.string.reais_0);
        b.valorInicial.addTextChangedListener(new MoneyTextWatcher(b.valorInicial));
        b.valorInicial.requestFocus();

        b.valorPoupanca.setTextColor(COLORS[0]);
        b.valorPoupancaHeader.setTextColor(COLORS[0]);
        b.valorAzul.setTextColor(COLORS[1]);
        b.valorAzulHeader.setTextColor(COLORS[1]);
        b.valorInicial.setText(FmtUtils.realValue(VALOR_INICIAL));

        b.valorAzulHeader.setText("INVISTA");
        b.cdi.setText(getRateSufix());

        final double saldoAnterior = getIntent().getDoubleExtra(SALDO, 0.0f);
        b.toolbar.setSubtitle("Saldo: " +FmtUtils.realValue(saldoAnterior));

        b.botaoAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BigDecimal qntParaInvestir  = FmtUtils.toBigDecimal(getValorInicial());
                BigDecimal qntAnterior      = FmtUtils.toBigDecimal((float)saldoAnterior);
                BigDecimal novoSaldo        = qntAnterior.subtract(qntParaInvestir);

                goToInvestirActivity(novoSaldo);
            }
        });

    }

    public void configToolbar() {
        Toolbar myChildToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myChildToolbar);
        ActionBar ab = getSupportActionBar();
        if(ab != null){
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }

    View.OnClickListener onClickContinuar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(viewModel.rates.getValue() == null){
                AlertAndroid.showMessageDialog(activity, new UnknownHostException());
                Log.d(TAG, "Animação não iniciada por falta de dados.");
                loadData();
            }else if(getValorInicial() < 0.01){
                b.valorInicial.setError(getString(R.string.erro_valor_zero));
            }else {
                anim.second();
            }
        }
    };

    TextView.OnEditorActionListener valorInicialActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if(!anim.second()){
                setupChart();
            }
            return false;
        }
    };

    private float vlPoupanca = 0;
    private float vInvest = 0;


    private class Animations{
        void first(){
            b.question.animate()
                    .alpha(1)
                    .setDuration(300)
                    .setInterpolator(new LinearInterpolator())
                    .start();
            b.secondLayer.animate()
                    .alpha(1)
                    .setDuration(300)
                    .setInterpolator(new LinearInterpolator())
                    .start();
        }

        boolean doSecondAnimation = true;
        boolean second(){
            boolean executed = false;
            if(doSecondAnimation){
                executed = true;
                ActivityUtils.hideKeyboard(SimuladorActivity.this);
                b.valorInicial.setBackground(getResources()
                        .getDrawable(R.drawable.valor_inicial_gray_bg));
                b.valorInicial.setTextColor(getResources().getColor(R.color.branco));
                b.question.animate()
                        .alpha(0)
                        .setDuration(300)
                        .setInterpolator(new LinearInterpolator())
                        .start();
                b.continuar.animate()
                        .alpha(0)
                        .setDuration(300)
                        .setInterpolator(new LinearInterpolator())
                        .start();
                b.secondLayer.animate()
                        .y(10)
                        .setDuration(300)
                        .setInterpolator(new LinearInterpolator())
                        .start();
                b.background.animate()
                        .alpha(1)
                        .setDuration(300)
                        .setInterpolator(new LinearInterpolator())
                        .start();

                int slHeight = b.secondLayer.getMeasuredHeight();
                b.labels.animate()
                        .y(slHeight)
                        .setDuration(300)
                        .setInterpolator(new LinearInterpolator())
                        .start();

                final float btnAplY = b.botaoAplicar.getY() - b.secondLayer.getHeight();
                final float chartYPlusHeight = b.barChart.getY() + b.barChart.getHeight();
                final float seekAndLabelY =  (btnAplY + chartYPlusHeight) / 2;
                b.seekAndLabel.animate()
                        .y(seekAndLabelY)
                        .alpha(1)
                        .setDuration(300)
                        .setInterpolator(new LinearInterpolator())
                        .setListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                b.labelArraste.setVisibility(View.VISIBLE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        })
                        .start();

                doSecondAnimation = false;
            }
            return executed;
        }

        final ScaleAnimation scaleUpAnim = new ScaleAnimation(
                1f, 1.5f , 1f, 1.5f,
                Animation.RELATIVE_TO_SELF, 1f,
                Animation.RELATIVE_TO_SELF, 0.5f);

        boolean isScaledUp = false;
        boolean isScalingUp = false;
        boolean isScalingDown = false;

        void startScrollSeekBar(){

            b.labels.animate()
                    .alpha(1)
                    .setDuration(300)
                    .setInterpolator(new LinearInterpolator())
                    .start();

            b.botaoAplicar.animate()
                    .alpha(1)
                    .setDuration(300)
                    .setInterpolator(new LinearInterpolator())
                    .start();
            b.botaoAplicar.setEnabled(true);

            if(!isScaledUp && !isScalingUp){
                Log.d("SCALING", "UP");
                isScalingUp = true;

                scaleUpAnim.setDuration(500);
                scaleUpAnim.setFillAfter(true);
                scaleUpAnim.setInterpolator(new LinearInterpolator());
                scaleUpAnim.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        isScaledUp = true;
                        isScalingUp = false;
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                b.dirValorPrincipal.startAnimation(scaleUpAnim);
                b.valorInicial.setAlpha(1);
                b.valorInicial.animate()
                        .alpha(0)
                        .setDuration(500)
                        .setInterpolator(new LinearInterpolator())
                        .start();
            }
        }

        void stopScrollSeekBar(){
            Handler handler = new Handler();
            final Runnable r = new Runnable() {
                public void run() {
                    if(isScaledUp && !isScalingDown){
                        Log.d("SCALING", "DOWN");
                        isScalingDown = true;
                        final ScaleAnimation anim = new ScaleAnimation(
                                1.5f, 1f , 1.5f, 1.1f,
                                Animation.RELATIVE_TO_SELF, 1f,
                                Animation.RELATIVE_TO_SELF, 0.5f);
                        anim.setDuration(DURATION_MILLIS);
                        anim.setFillAfter(true);
                        anim.setInterpolator(new LinearInterpolator());
                        anim.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                isScaledUp = false;
                                isScalingDown = false;
                                isScalingUp = false;
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                        b.dirValorPrincipal.startAnimation(anim);
                        b.valorInicial.animate()
                                .alpha(1)
                                .setDuration(500)
                                .setInterpolator(new LinearInterpolator())
                                .start();
                    }
                }
            };
            handler.postDelayed(r, 2*DURATION_MILLIS);

            anim.fadeInChart();

        }

        void fadeInChart() {
            b.barChart.animate()
                    .alpha(1)
                    .setDuration(300)
                    .setInterpolator(new LinearInterpolator())
                    .start();
        }
    }

    public void loadData() {
        loadRates(stockType, UserPrefs.getProfile(this).customerId);
    }

    public void loadRates(int stockType, String customerId){
        GetStockRates.RequestParams params = new GetStockRates.RequestParams();
        params.customerId = customerId;
        params.stockType = String.valueOf(stockType);

        GetStockRates.ResponseHandler responseHandler = new GetStockRates.ResponseHandler() {
            @Override
            public void onSuccess(StockRatesTO data) {

                StockRatesTO.RateTO firstRate = new StockRatesTO.RateTO();
                firstRate.days = 0;
                firstRate.period = 0;
                firstRate.percentageCDI = 0.0f;
                firstRate.savingsPercentage = 0.0f;
                firstRate.savingsRate = 0.0f;
                firstRate.rate = 0.0f;

                data.rates.add(0, firstRate);


                viewModel.minRate = data.rates.get(0);
                b.slider.setMax(data.rates.size()-1);
                viewModel.rates.setValue(data.rates);
            }

            @Override
            public void onUnsuccess(RetornoWS<StockRatesTO> retornoWS) {

            }

            @Override
            public void onError(IOException iOException) {
                viewModel.rates.setValue(null);
            }
        };

        new GetStockRates()
                .setRensponseHandler(responseHandler)
                .setRequestParams(params)
                .execute();
    }

    private void goToInvestirActivity(BigDecimal novoSaldo) {
        Intent intent = new Intent(activity, InvestirActivity.class);
        intent.putExtra(STOCK_TYPE, stockType);
        intent.putExtra(PERIOD, selectedRate.days);
        intent.putExtra(NOME_APLICACAO,getString(R.string.rdb_avista_financeira) );
        intent.putExtra(VALOR_APLICADO, getValorInicial() );
        intent.putExtra(DATA_VENCIMENTO, selectedRate.expirationDate );
        intent.putExtra(REMUNERACAO, FmtUtils.percent(selectedRate.percentageCDI) + getRateSufix());
        intent.putExtra(SALDO, FmtUtils.realValue(novoSaldo.doubleValue()));
        intent.putExtra(SAVINGS_PERCENTAGE, selectedRate.savingsPercentage);
        activity.startActivity(intent);
    }

    @NonNull
    private String getRateSufix() {
        String s = stockType != POS_FIXADO ? getString(R.string.ao_ano) : getString(R.string.do_cdi);
        return " " + s;
    }

    public void setupChart(){

        if(selectedRate == null){
            return;
        }

        if(selectedRate.period.equals(0) && b.continuar.getAlpha() < 0.1){
            b.barChart.setVisibility(View.INVISIBLE);
            b.labelArraste.setVisibility(View.VISIBLE);
            b.valorAzulHeader.setVisibility(View.INVISIBLE);
            b.valorAzul.setVisibility(View.INVISIBLE);
            b.valorPoupancaHeader.setVisibility(View.INVISIBLE);
            b.valorPoupanca.setVisibility(View.INVISIBLE);
            b.botaoAplicar.setVisibility(View.INVISIBLE);
        }else{
            b.barChart.setVisibility(View.VISIBLE);
            b.labelArraste.setVisibility(View.INVISIBLE);
            b.valorAzulHeader.setVisibility(View.VISIBLE);
            b.valorAzul.setVisibility(View.VISIBLE);
            b.valorPoupancaHeader.setVisibility(View.VISIBLE);
            b.valorPoupanca.setVisibility(View.VISIBLE);
            b.botaoAplicar.setVisibility(View.VISIBLE);
        }

        float pv = getValorInicial();
        ArrayList<BarEntry> barentry = new ArrayList<>();
        ArrayList<String> barEntryLabels = new ArrayList<>();
        BarDataSet bardataset ;
        BarData bardata ;

        vlPoupanca = (pv * selectedRate.savingsRate) - pv;
        barentry.add(new BarEntry(1, vlPoupanca));
        b.valorPoupanca.setText(getString(R.string.a_mais_lucro, FmtUtils.realValue((double) vlPoupanca)));

        vInvest = (pv * selectedRate.rate) - pv;
        barentry.add(new BarEntry(2, vInvest));
        b.valorAzul.setText(getString(R.string.a_mais_lucro, FmtUtils.realValue((double) vInvest)));

        barEntryLabels.add(getString(R.string.poupanca));
        barEntryLabels.add(getString(R.string.rdb_avista_financeira));

        bardataset = new BarDataSet(barentry, "Rendimentos");
        bardata = new BarData(bardataset);
        bardataset.setColors(COLORS);
        bardataset.setDrawValues(false);

        b.barChart.setData(bardata);
        b.barChart.getDescription().setEnabled(false);
        b.barChart.getLegend().setEnabled(false);
        b.barChart.animateY(DURATION_MILLIS);

        b.barChart.getXAxis().setEnabled(false);

        b.barChart.setScaleEnabled(false);

        b.barChart.getAxisLeft().setAxisMinimum(0);
        b.barChart.getAxisRight().setAxisMinimum(0);

        RectF barraPoupanca = b.barChart
                .getBarBounds(b.barChart.getBarData().getDataSets().get(0).getEntryForIndex(0));
        alignByView(b.valorPoupanca, barraPoupanca);
        alignByView(b.valorPoupancaHeader, barraPoupanca);

        RectF barraAzul = b.barChart
                .getBarBounds(b.barChart.getBarData().getDataSets().get(0).getEntryForIndex(1));
        alignByView(b.valorAzul, barraAzul);
        alignByView(b.valorAzulHeader, barraAzul);
    }

    /**
     * Alinha os labels situados acima do gráfico, com as barras do gráfico de forma centralizado
     * @param toAlign label que será alinhado á barra
     * @param by barra do  gráfico
     */
    private void alignByView(View toAlign, RectF by){
        toAlign.setX(by.centerX() - toAlign.getWidth()/2);
    }

    private void setupSeekBar() {
        b.slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                b.labelArraste.setVisibility(View.INVISIBLE);

                StockRatesTO.RateTO taxa = viewModel.rates.getValue().get(progress);
                if(taxa != null){
                    selectedRate = taxa;
                    b.nMeses.setText(String.valueOf(taxa.period));
                    b.taxa.setText(FmtUtils.percent(selectedRate.percentageCDI));
                    b.cdi.setText(getRateSufix());
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                anim.startScrollSeekBar();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                anim.stopScrollSeekBar();
                setupChart();
            }
        });
    }

    private float getValorInicial() {
        String value = b.valorInicial.getText().toString();
        try {
            return FmtUtils.REAL_FORMATTER.parse(value).floatValue();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0.0f;
    }
}
