package br.com.meupag.investimentos.features.simulador.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.call.GetStockRates;
import br.com.meupag.investimentos.ws.transferobject.StockRatesTO;
import br.com.meupag.investimentos.util.FmtUtils;

public class SimuladorViewModel extends ViewModel {

    public final MutableLiveData<List<StockRatesTO.RateTO>> rates = new MutableLiveData<>();

    public StockRatesTO.RateTO minRate;

    public SimuladorViewModel() {
    }

    public StockRatesTO.RateTO getMinRate() {
        return minRate;
    }
}
