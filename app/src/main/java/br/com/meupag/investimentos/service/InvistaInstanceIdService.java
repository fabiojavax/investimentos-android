package br.com.meupag.investimentos.service;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class InvistaInstanceIdService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {

        try {

            String refreshedToken = FirebaseInstanceId.getInstance().getToken();

            if (refreshedToken != null && refreshedToken.trim().length() > 0) {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
                final SharedPreferences.Editor edit = prefs.edit();
                edit.putString("PREF_USER_TOKEN", refreshedToken);
                edit.apply();

            }

        } catch (Exception e) {

            e.printStackTrace();

        }

    }

}