/*
    Esse código-fonte pertence à ORUS ON-LINE LTDA, CNPJ 14.860.302/0001-44
    e não pode ser copiado, consultado ou utilizado, para fins pessoais ou de terceiros,
    sem a expressa autorização por escrito da mesma. A não observância desses termos implicará nas
    penalidades previstas na LEI Nº 9.609/98
 */

package br.com.meupag.investimentos.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import br.com.meupag.investimentos.ws.transferobject.ProfileTO;

public class UserPrefs {

    //TODO alterar foto

    private static final String PREF_CUSTOMER_ID = "pref_customer_id";
    private static final String PREF_LAST_CUSTOMER_ID = "pref_last_customer_id";
    private static final String PREF_NAME = "pref_name";
    private static final String PREF_EMAIL = "pref_email";
    private static final String PREF_PHONE = "pref_phone";
    private static final String PREF_BANK_CODE = "pref_bank_code";
    private static final String PREF_ACCOUNT_NUMBER = "pref_account_number";
    private static final String PREF_ACCOUNT_NUMBER_CFI = "pref_account_number_cfi";
    private static final String PREF_BANK_AGENCY = "pref_bank_agency";
    private static final String PREF_ACCOUNT_TYPE = "pref_account_type";
    private static final String PREF_SIGNATURE = "pref_signature";
    private static final String PREF_PASSWORD = "pref_password";
    private static final String PREF_BALANCE = "pref_balance";

    private static final String PREF_ACCOUNT_BALLANCE = "pref_account_ballance";

    public static ProfileTO getProfile(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String login = prefs.getString(PREF_CUSTOMER_ID, null);
        if (login == null) {
            return new ProfileTO();
        }

        ProfileTO userTO = new ProfileTO();

        userTO.customerId = login;
        userTO.name = prefs.getString(PREF_NAME, null);
        userTO.email = prefs.getString(PREF_EMAIL, null);
        userTO.phone = prefs.getString(PREF_PHONE, null);
        userTO.bankInfo.bankCode = prefs.getString(PREF_BANK_CODE, null);
        userTO.bankInfo.accountNumber = prefs.getString(PREF_ACCOUNT_NUMBER, null);
        userTO.bankInfo.accountType = prefs.getString(PREF_ACCOUNT_TYPE, null);
        userTO.bankInfo.bankAgency = prefs.getString(PREF_BANK_AGENCY, null);
        userTO.signature = prefs.getString(PREF_SIGNATURE, null);

        return userTO;
    }

    public static String getSignature(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(PREF_SIGNATURE, null);
    }

    public static String getPassword(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(PREF_PASSWORD, null);
    }

    public static String getBalance(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(PREF_BALANCE, null);
    }

    public static String getPrefAccountNumberCfi(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(PREF_ACCOUNT_NUMBER_CFI, null);
    }

    public static String getPrefLastCustomerId(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString(PREF_LAST_CUSTOMER_ID, null);
    }

    public static void setProfile(Context context, ProfileTO profileTO) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor edit = prefs.edit();

        edit.putString(PREF_CUSTOMER_ID, profileTO.customerId);
        edit.putString(PREF_NAME, profileTO.name);
        edit.putString(PREF_EMAIL, profileTO.email);
        edit.putString(PREF_PHONE, profileTO.phone);
        edit.putString(PREF_BANK_CODE, profileTO.bankInfo.bankCode);
        edit.putString(PREF_BANK_AGENCY, profileTO.bankInfo.bankAgency);
        edit.putString(PREF_ACCOUNT_NUMBER, profileTO.bankInfo.accountNumber);
        edit.putString(PREF_ACCOUNT_TYPE, profileTO.bankInfo.accountType);

        edit.apply();
    }

    public static void setSignature(Context context, String signature) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor edit = prefs.edit();
        edit.putString(PREF_SIGNATURE, signature);

        edit.apply();
    }

    public static void setLastUsedCustomerId(Context context, String lastUsedCustomerId) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor edit = prefs.edit();
        edit.putString(PREF_LAST_CUSTOMER_ID, lastUsedCustomerId);

        edit.apply();
    }

    public static void setPassword(Context context, String password) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor edit = prefs.edit();
        edit.putString(PREF_PASSWORD, password);

        edit.apply();
    }

    public static void setAccountNumber(Context context, String accountNumber) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor edit = prefs.edit();
        edit.putString(PREF_ACCOUNT_NUMBER_CFI, accountNumber);

        edit.apply();
    }

    public static void setBalance(Context context, Double balance) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor edit = prefs.edit();
        edit.putFloat(PREF_BALANCE, balance.floatValue());

        edit.apply();
    }

    public static void clear(Context context) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor edit = prefs.edit();

        edit.remove(PREF_CUSTOMER_ID);
        edit.remove(PREF_NAME);
        edit.remove(PREF_EMAIL);
        edit.remove(PREF_PHONE);

        edit.remove(PREF_BANK_CODE);
        edit.remove(PREF_BANK_AGENCY);
        edit.remove(PREF_ACCOUNT_TYPE);
        edit.remove(PREF_ACCOUNT_NUMBER);

        edit.remove(PREF_SIGNATURE);
        edit.remove(PREF_PASSWORD);

        edit.remove(PREF_BALANCE);

        edit.remove(PREF_ACCOUNT_NUMBER_CFI);

        edit.apply();

    }


}
