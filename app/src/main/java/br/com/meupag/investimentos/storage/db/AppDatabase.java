package br.com.meupag.investimentos.storage.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import br.com.meupag.investimentos.storage.model.Domain;

@Database(entities = {Domain.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract DomainDAO getDomainDAO();
}
