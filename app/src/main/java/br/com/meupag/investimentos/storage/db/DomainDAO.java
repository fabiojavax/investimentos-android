package br.com.meupag.investimentos.storage.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import br.com.meupag.investimentos.storage.model.Domain;

@Dao
public interface DomainDAO {

    @Insert
    void insert(Domain... domains);

    @Update
    void update(Domain... domains);

    @Delete
    void delete(Domain domain);

    @Query("SELECT * FROM "+Domain.TABLE+" WHERE "+Domain.COL_CODE+" = :code AND TYPE = :type")
    Domain getDomainByTypeWithCodigo(int type, String code);

    @Query("SELECT * FROM "+Domain.TABLE)
    List<Domain> getBanks();
}
