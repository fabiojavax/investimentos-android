package br.com.meupag.investimentos.storage.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import br.com.meupag.investimentos.ws.transferobject.ProductDomainTO;

import static br.com.meupag.investimentos.storage.model.Domain.TABLE;

@Entity(tableName = TABLE)
public class Domain {

    public static final String TABLE = "domain";
    public static final String COL_CODE = "code";
    public static final String COL_DESCRIPTION = "description";
    public static final String COL_TYPE = "type";

    public static final int BANK_TYPE = 0;
    public static final int ACCOUNT_TYPE = 1;

    @PrimaryKey
    @NonNull
    private Integer id;
    private String code;
    private String description;

    @NonNull
    private Integer type;

    public Domain() {
    }

    public Domain(ProductDomainTO.DomainTO bankDomainTO, int domainType) {
        this.code = bankDomainTO.code;
        this.description = bankDomainTO.description;
        this.type = domainType;
    }

    @NonNull
    public Integer getType() {
        return type;
    }

    public void setType(@NonNull Integer type) {
        this.type = type;
    }

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(@NonNull Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
