package br.com.meupag.investimentos.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import br.com.meupag.investimentos.R;
import br.com.meupag.investimentos.storage.UserPrefs;

public class ActivityUtils {
    public static void hideKeyboard(Activity activity) {
        if(activity != null){
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if(imm != null && view != null){
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }

    public interface ActionExecutor{
        void execute();
    }

    public static void configSignatureDialogToAction(final Activity act, View signatureCaller, final ActionExecutor actionExecutor) {
        final View view = act.getLayoutInflater().inflate(R.layout.alert_assinatura, null);
        final EditText assinaturaEditText = view.findViewById(R.id.et_assinatura);

        final AlertDialog dialogAssinatura = new AlertDialog.Builder(act)
                .setTitle(R.string.ass_eletronica)
                .setView(view)
                .setMessage(R.string.informe_ass_eletronica)
                .setNegativeButton(R.string.cancelar, null)
                .setPositiveButton(R.string.yes, null)
                .create();

        final View.OnClickListener onClickConfirmarInvestimento =
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String correctSignature = UserPrefs.getSignature(act);//pode ser null
                        String attempedSignature = assinaturaEditText.getText().toString();

                        if(!attempedSignature.equals(correctSignature)){
                            assinaturaEditText.setError(act.getString(R.string.erro_assinatura_nao_confere));
                        }else{
                            actionExecutor.execute();
                            dialogAssinatura.dismiss();
                        }
                    }
                };

        dialogAssinatura.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button positiveButton = dialogAssinatura.getButton(DialogInterface.BUTTON_POSITIVE);
                positiveButton.setOnClickListener(null);
                positiveButton.setOnClickListener(onClickConfirmarInvestimento);
            }
        });

        if(signatureCaller != null){
            signatureCaller.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogAssinatura.show();
                }
            });
        }else{
            dialogAssinatura.show();
        }

    }
}
