package br.com.meupag.investimentos.util;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.view.KeyEvent;

import java.io.IOException;
import java.net.UnknownHostException;

import br.com.meupag.investimentos.R;


public class AlertAndroid {

    public static void showMessageDialog(Context context, String message) {

        Builder confirmDialogBuilder = new Builder(context);
        confirmDialogBuilder.setCancelable(false);
        if (message != null) {
            confirmDialogBuilder.setMessage(message);
        }
        confirmDialogBuilder.setPositiveButton(R.string.ok, new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmDialogBuilder.setTitle(R.string.app_name);

        AlertDialog alert = confirmDialogBuilder.create();
        alert.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                    return true;
                }
                return false;
            }
        });

        alert.show();

    }

    public static void showMessageDialog(Context context, IOException iOException) {
        if(iOException instanceof UnknownHostException){

            Builder confirmDialogBuilder = new Builder(context);
            confirmDialogBuilder.setCancelable(false);
            confirmDialogBuilder.setMessage(context.getString(R.string.erro_sem_conexao));
            confirmDialogBuilder.setPositiveButton(R.string.ok, new OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            confirmDialogBuilder.setTitle(R.string.app_name);

            AlertDialog alert = confirmDialogBuilder.create();
            alert.setIcon(R.drawable.ic_signal_wifi_off_24dp);
            alert.setOnKeyListener(new DialogInterface.OnKeyListener() {

                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_SEARCH) {
                        return true;
                    }
                    return false;
                }
            });

            alert.show();

        }else{
            showMessageDialog(context, iOException.getMessage());
        }
    }

    public static void showConfirmDialog(Context context, int message, OnClickListener onClickListener) {

        AlertDialog.Builder confirmDialogBuilder = new AlertDialog.Builder(context);

        confirmDialogBuilder.setCancelable(false).setTitle(R.string.app_name).setMessage(message)
                .setPositiveButton(R.string.yes, onClickListener);

        AlertDialog alert = confirmDialogBuilder.create();
        alert.show();

    }

    public static void showConfirmDialog(Context context, String message, OnClickListener onClickListener) {

        AlertDialog.Builder confirmDialogBuilder = new AlertDialog.Builder(context);

        confirmDialogBuilder.setCancelable(false).setTitle(R.string.app_name).setMessage(message)
                .setPositiveButton(R.string.yes, onClickListener);

        AlertDialog alert = confirmDialogBuilder.create();
        alert.show();

    }


    public static void showRetryDialog(final Activity ctx,
                                       int retryMessage,
                                       OnClickListener onClickListenerPositive,
                                       OnClickListener onClickListenerNegative) {

        AlertDialog.Builder confirmDialogBuilder = new AlertDialog.Builder(ctx);

        confirmDialogBuilder.setCancelable(false).setTitle(R.string.impossivel_atualizar_dados).setMessage(retryMessage)
                .setPositiveButton(R.string.tentar_novamente, onClickListenerPositive)
                .setNegativeButton(R.string.cancelar, onClickListenerNegative);

        AlertDialog alert = confirmDialogBuilder.create();
        alert.show();
    }

}
