package br.com.meupag.investimentos.util;

import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class FmtUtils {
    private static final String TAG = FmtUtils.class.getSimpleName();

    @SuppressWarnings("WeakerAccess")
    public static Locale BR_LOCALE = new Locale("pt", "BR");

    public static DateFormat YYYY_MM_DD = new SimpleDateFormat("yyyy-MM-dd", BR_LOCALE);
    public static DateFormat DD_MM_YYYY = new SimpleDateFormat("dd/MM/yyyy", BR_LOCALE);

    public static NumberFormat REAL_FORMATTER = NumberFormat.getCurrencyInstance(BR_LOCALE);

    public static String realValue(Double valor){
        return REAL_FORMATTER.format(valor);
    }

    public static String percent(Double valor){
        return String.format(BR_LOCALE, "%.2f%%", valor);
    }

    public static String percent(Float valor){
        return String.format(BR_LOCALE, "%.2f%%", valor);
    }

    public static String dateUStoBR(String dataVencimento) {
        try {
            return FmtUtils.DD_MM_YYYY.format(FmtUtils.YYYY_MM_DD.parse(dataVencimento));
        } catch (ParseException e) {
            Log.d(TAG, "Erro ao converter formato de data: " + dataVencimento);
            return dataVencimento;
        }
    }

    public static String dateBRtoUS(String dataVencimento) {
        try {
            return FmtUtils.YYYY_MM_DD.format(FmtUtils.DD_MM_YYYY.parse(dataVencimento));
        } catch (ParseException e) {
            Log.d(TAG, "Erro ao converter formato de data: " + dataVencimento);
            return dataVencimento;
        }
    }

    public static BigDecimal parse(final String amount) throws ParseException {
        final NumberFormat format = NumberFormat.getNumberInstance(BR_LOCALE);
        if (format instanceof DecimalFormat) {
            ((DecimalFormat) format).setParseBigDecimal(true);
        }
        return (BigDecimal) format.parse(amount.replaceAll("[^\\d.,]",""));
    }


    public static BigDecimal toBigDecimal(float valorAplicado) {
        return new BigDecimal(valorAplicado).setScale(2, RoundingMode.HALF_DOWN);
    }
}
