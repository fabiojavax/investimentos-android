package br.com.meupag.investimentos.util;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;

public class HttpHelper {
    public static final String UTF8 = "UTF-8";
    private static int TIMEOUT = '\uea60';

    public static final OkHttpClient OK_HTTP_CLIENT = new OkHttpClient.Builder()
            .readTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .build();

    public static final MediaType JSON_MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8");

    public HttpHelper() {
    }

    public static String doGet(String url, String charset) throws IOException {
        URL u = new URL(url);
        HttpURLConnection conn = (HttpURLConnection)u.openConnection();
        conn.setReadTimeout(TIMEOUT);
        conn.setConnectTimeout(TIMEOUT);
        conn.setRequestMethod("GET");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        conn.connect();
        InputStream in = conn.getInputStream();
        String s = IOUtils.toString(in, charset);
        in.close();
        conn.disconnect();
        return s;
    }

    public static String requestHttpGet(String url, Map<String, String> param) throws IOException {
        return requestHttpGet(url, (Map)param, (Map)null);
    }

    public static String requestHttpGet(String url, Map<String, String> param, Map<String, String> headers) throws  IOException {
        StringBuilder urlComParam = new StringBuilder(url);
        urlComParam.append("?");
        urlComParam.append(getQueryString(param));
        return requestHttpGet(urlComParam.toString(), "UTF-8", headers);
    }

    public static String requestHttpGet(String url) throws  IOException {
        return requestHttpGet(url, "UTF-8");
    }

    public static String requestHttpGet(String url, String charset) throws  IOException {
        return requestHttpGet(url, (String)charset, (Map)null);
    }

    public static String requestHttpGet(String url, String charset, Map<String, String> headers) throws  IOException {
        String result = "";
        InputStream is = null;
        DefaultHttpClient httpclient = new DefaultHttpClient();
        HttpGet httpget = new HttpGet(url);
        if(headers != null) {
            Iterator entity = headers.entrySet().iterator();

            while(entity.hasNext()) {
                Entry response = (Entry)entity.next();
                httpget.addHeader((String)response.getKey(), (String)response.getValue());
            }
        }

        HttpResponse response1 = httpclient.execute(httpget);
        HttpEntity entity1 = response1.getEntity();
        is = entity1.getContent();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is, charset));
        StringBuilder sb = new StringBuilder();
        String line = null;

        while((line = reader.readLine()) != null) {
            sb.append(line + "\n");
        }

        is.close();
        result = sb.toString();
        return result;
    }

    public static String doPost(String url, Map<String, String> params) throws IOException {
        return doPost(url, (Map)params, "UTF-8", (Map)null);
    }

    public static String doPost(String url, Map<String, String> params, Map<String, String> headers) throws IOException {
        return doPost(url, params, "UTF-8", headers);
    }

    public static String doPost(String url, Map<String, String> params, String charset) throws IOException {
        return doPost(url, (Map)params, charset, (Map)null);
    }

    public static String doPost(String url, Map<String, String> params, String charset, Map<String, String> headers) throws IOException {
        String queryString = getQueryString(params, charset);
        String texto = doPost(url, queryString, charset, headers);
        return texto;
    }

    public static String doPost(String url, String params, String charset) throws IOException {
        return doPost(url, (String)params, charset, (Map)null);
    }

    public static String doPost(String url, String params, String charset, Map<String, String> headers) throws IOException {
        URL u = new URL(url);
        HttpURLConnection conn = (HttpURLConnection)u.openConnection();
        conn.setReadTimeout(TIMEOUT);
        conn.setConnectTimeout(TIMEOUT);
        conn.setRequestMethod("POST");
        conn.setDoOutput(true);
        conn.setDoInput(true);
        if(headers != null) {
            Iterator s = headers.entrySet().iterator();

            while(s.hasNext()) {
                Entry in = (Entry)s.next();
                conn.setRequestProperty((String)in.getKey(), (String)in.getValue());
            }
        }

        conn.connect();
        if(params != null) {
            OutputStream in1 = conn.getOutputStream();
            byte[] s1 = params.getBytes(charset);
            in1.write(s1);
            in1.flush();
            in1.close();
        }

        InputStream in2 = conn.getInputStream();
        String s2 = IOUtils.toString(in2, charset);

        try {
            conn.disconnect();
        } catch (Exception var10) {
            var10.printStackTrace();
        }

        try {
            if(in2 != null) {
                in2.close();
            }
        } catch (Exception var9) {
            var9.printStackTrace();
        }

        return s2;
    }

    public static String getQueryString(Map<String, String> params) throws IOException {
        return getQueryString(params, "UTF-8");
    }

    public static String getQueryString(Map<String, String> params, String charsetToEncode) throws IOException {
        if(params != null && params.size() != 0) {
            String urlParams = null;
            Iterator var4 = params.keySet().iterator();

            while(var4.hasNext()) {
                String chave = (String)var4.next();
                Object objValor = params.get(chave);
                if(objValor != null) {
                    String valor = objValor.toString();
                    if(charsetToEncode != null) {
                        valor = URLEncoder.encode(valor, charsetToEncode);
                    }

                    urlParams = urlParams == null?"":urlParams + "&";
                    urlParams = urlParams + chave + "=" + valor;
                }
            }

            return urlParams;
        } else {
            return null;
        }
    }
}

