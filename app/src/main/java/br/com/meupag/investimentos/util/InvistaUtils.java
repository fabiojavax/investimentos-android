package br.com.meupag.investimentos.util;

public class InvistaUtils {

    public static String taxa(Double taxa) {
        String remuneracao = tipoTaxa(taxa);
        return FmtUtils.percent(taxa) + remuneracao;
    }

    public static String tipoTaxa(Double taxa) {
        return taxa > 50 ? " do CDI" : " ao Ano";
    }
}
