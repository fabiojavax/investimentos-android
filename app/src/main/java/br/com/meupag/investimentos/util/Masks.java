package br.com.meupag.investimentos.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public abstract class Masks {

    private static final String telefoneMask = "(##) ####-####";
    private static final String celularMask = "(##) #####-####";

    private static final String maskCEP = "##.###-###";

    private static final String maskDATA = "##/##/####";


    public static String unmask(String s) {
        return s.replaceAll("[^0-9]*", "");
    }

    public static void setCelularMask(final EditText editText){
         editText.addTextChangedListener(insert(editText, celularMask));
    }

    public static void setCepMask(final EditText editText){
        editText.addTextChangedListener(insert(editText, maskCEP));
    }

    public static void setDataMask(final EditText editText){
        editText.addTextChangedListener(insert(editText, maskDATA));
    }


    public static TextWatcher insert(final EditText editText, final String m) {
        return new TextWatcher() {
            boolean isUpdating;
            String old = "";
            String mask = m;

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = Masks.unmask(s.toString());
                String mascara = mask;

                if(mascara.equals(telefoneMask) || mascara.equals(celularMask)){
                    if(str.length() <= 10){
                        mascara = telefoneMask;
                    }else{
                        mascara = celularMask;
                    }
                }

                String result = "";
                if (isUpdating) {
                    old = str;
                    isUpdating = false;
                    return;
                }
                int i = 0;
                for (char m : mascara.toCharArray()) {
                    if ((m != '#' && str.length() > old.length()) || (m != '#' && str.length() < old.length() && str.length() != i)) {
                        result += m;
                        continue;
                    }

                    try {
                        result += str.charAt(i);
                    } catch (Exception e) {
                        break;
                    }
                    i++;
                }
                isUpdating = true;
                editText.setText(result);
                editText.setSelection(result.length());
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        };
    }
}
