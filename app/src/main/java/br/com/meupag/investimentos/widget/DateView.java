package br.com.meupag.investimentos.widget;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import br.com.meupag.investimentos.util.FmtUtils;

public class DateView extends AppCompatTextView {
    public DateView(Context context) {
        super(context);
        setDate(Calendar.getInstance().getTime());
    }

    public DateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setDate(Calendar.getInstance().getTime());
    }

    public DateView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setDate(Calendar.getInstance().getTime());
    }

    public void setDate(Date date) {
        String today = FmtUtils.DD_MM_YYYY.format(date);
        setText(today);
    }

}
