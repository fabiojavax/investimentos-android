package br.com.meupag.investimentos.ws.call;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.ws.transferobject.AccountTO;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class GetCustomerAccount extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = GetCustomerAccount.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_CUSTOMER_ACCOUNT;
    private static final Class<AccountTO> clazz = AccountTO.class;

    RetornoWS<AccountTO> retornoWS;
    IOException ioException;

    private ResponseHandler responseHandler;
    private RequestParams requestParams;

    public static class RequestParams{
        public String customerId;
    }

    public interface ResponseHandler{
        void onSuccess(AccountTO data);
        void onUnsuccess(RetornoWS<AccountTO> retornoWS);
        void onError(IOException iOException);
    }

    public GetCustomerAccount setRensponseHandler(ResponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    public GetCustomerAccount setRequestParams(RequestParams requestParams) {
        this.requestParams = requestParams;
        return this;
    }

    private String paramsToQueryString(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(URL).newBuilder();
        urlBuilder.addQueryParameter("customerId", requestParams.customerId);
        return urlBuilder.build().toString();
    }

    @Override
    protected void onPreExecute() {
        if(responseHandler == null){
            Log.d(TAG, "Favor implementar o ResponseHandler");
            GetCustomerAccount.this.cancel(true);
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{

            Request request = new Request.Builder()
                    .url(paramsToQueryString())
                    .get()
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = OK_HTTP_CLIENT.newCall(request).execute();

            if(response.isSuccessful()){
                ResponseBody responseBody = response.body();

                if(responseBody == null){
                    return false;
                }

                final String jsonResponse = responseBody.string();
                Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

                ObjectMapper mapper = Constantes.mapper;
                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, clazz);
                retornoWS = mapper.readValue(jsonResponse, type);

                return retornoWS.getStatus();
            } else {
                return false;
            }


        }catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {

        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS.getData());
        } else {
            Log.e(TAG, "Não foi possível recuperar AccountTO\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }


    private static GetCustomerAccount getCustomerAccount;

    public static void doCall(Activity act, GetCustomerAccount.RequestParams rp,
                              GetCustomerAccount.ResponseHandler rh){
        if(getCustomerAccount != null){
            getCustomerAccount.cancel(true);
        }
        getCustomerAccount = new GetCustomerAccount();
        getCustomerAccount.setRensponseHandler(rh);
        getCustomerAccount.setRequestParams(rp);
        getCustomerAccount.execute();
    }
}
