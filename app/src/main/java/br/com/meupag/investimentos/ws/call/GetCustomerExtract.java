package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.ws.transferobject.ExtractTO;
import br.com.meupag.investimentos.util.HttpHelper;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class GetCustomerExtract extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = GetCustomerExtract.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_CUSTOMER_EXTRACT;
    private static final Class<ExtractTO> clazz = ExtractTO.class;

    private final OkHttpClient okHttpClient = HttpHelper.OK_HTTP_CLIENT;

    ResponseHandler responseHandler;
    RetornoWS<ExtractTO> retornoWS;
    IOException ioException;

    public final RequestParams requestParams = new RequestParams();

    public static class RequestParams{
        final String authKey = Constantes.AUTH_KEY;
        public String customerId;
    }

    public interface ResponseHandler{
        void onSuccess(ExtractTO data);
        void onUnsuccess(RetornoWS<ExtractTO> retornoWS);
        void onError(IOException ioException);
    }

    public GetCustomerExtract setRensponseHandler(ResponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    private String paramsToQueryString(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(URL).newBuilder();
        urlBuilder.addQueryParameter("customerId", requestParams.customerId);
        urlBuilder.addQueryParameter("authKey", requestParams.authKey);
        return urlBuilder.build().toString();
    }


    @Override
    protected Boolean doInBackground(Void... voids) {

        try{

            Request request = new Request.Builder()
                    .url(paramsToQueryString())
                    .get()
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = okHttpClient.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if(responseBody != null){
                final String jsonResponse = responseBody.string();
                Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

                ObjectMapper mapper = Constantes.mapper;
                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, ExtractTO.class);
                retornoWS = mapper.readValue(jsonResponse, type);

                return retornoWS.getStatus();

            }

        }catch (IOException e){
            this.ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(success != null && responseHandler != null){
            if(ioException != null){
                responseHandler.onError(ioException);
                return;
            }

            if(success){
                responseHandler.onSuccess(retornoWS.getData());
            } else {
                Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
                responseHandler.onUnsuccess(retornoWS);
            }
        }
    }
}
