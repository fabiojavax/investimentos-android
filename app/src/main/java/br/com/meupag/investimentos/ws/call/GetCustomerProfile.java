package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class GetCustomerProfile extends AsyncTask<Void, String, Boolean> {
    private static final String TAG = GetCustomerProfile.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_CUSTOMER_PROFILE;

    private ResponseHandler responseHandler;
    RetornoWS<ProfileTO> retornoWS;

    public final RequestParams requestParams = new RequestParams();

    public static class RequestParams{
        public String customerId;
    }

    public interface ResponseHandler{
        void onSuccess(ProfileTO data);
        void onError(RetornoWS<ProfileTO> retornoWS);
    }

    public GetCustomerProfile setRensponseHandler(ResponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    private String paramsToQueryString(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(URL).newBuilder();
        urlBuilder.addQueryParameter("customerId", requestParams.customerId);
        return urlBuilder.build().toString();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{

            Request request = new Request.Builder()
                    .url(paramsToQueryString())
                    .get()
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = OK_HTTP_CLIENT.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if(responseBody != null){
                final String jsonResponse = responseBody.string();
                Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

                final ObjectMapper mapper = Constantes.mapper;
                final JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, ProfileTO.class);
                retornoWS = mapper.readValue(jsonResponse, type);

                return retornoWS.getStatus();
            }


        }catch (IOException e){
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(responseHandler != null && success){
            responseHandler.onSuccess(retornoWS.getData());
        }else if(success == null || !success){
            Log.e(TAG, "Não foi possível recuperar ProfileTO" + retornoWS.getMessage());
        }
    }
}
