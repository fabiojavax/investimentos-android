package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Date;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.FmtUtils;
import br.com.meupag.investimentos.ws.transferobject.WalletTO;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class GetCustomerWallet extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = GetCustomerWallet.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_CUSTOMER_WALLET;
    private static final Class<WalletTO> clazz = WalletTO.class;

    private ResponseHandler responseHandler;

    RetornoWS<WalletTO> retornoWS;
    IOException ioException;

    public final RequestParams requestParams = new RequestParams();

    public static class RequestParams{
        final String authKey = Constantes.AUTH_KEY;
        final String referenceDate = FmtUtils.YYYY_MM_DD.format(new Date());
        public String customerId;
    }

    public interface ResponseHandler{
        void onSuccess(WalletTO data);
        void onUnsuccess(RetornoWS<WalletTO> retornoWS);
        void onError(IOException iOException);
    }

    public GetCustomerWallet setRensponseHandler(ResponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    private String paramsToQueryString(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(URL).newBuilder();
        urlBuilder.addQueryParameter("customerId", requestParams.customerId);
        urlBuilder.addQueryParameter("referenceDate", requestParams.referenceDate);
        urlBuilder.addQueryParameter("authKey", requestParams.authKey);
        return urlBuilder.build().toString();
    }


    @Override
    protected Boolean doInBackground(Void... voids) {

        try{

            Request request = new Request.Builder()
                    .url(paramsToQueryString())
                    .get()
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = OK_HTTP_CLIENT.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if(responseBody != null){
                final String jsonResponse = responseBody.string();
                Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

                ObjectMapper mapper = Constantes.mapper;
                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, WalletTO.class);
                retornoWS = mapper.readValue(jsonResponse, type);

                return retornoWS.getStatus();
            }


        }catch (IOException e){
            this.ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(success != null && responseHandler != null){
            if(ioException != null){
                responseHandler.onError(ioException);
                return;
            }

            if(success){
                responseHandler.onSuccess(retornoWS.getData());
            } else {
                Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
                responseHandler.onUnsuccess(retornoWS);
            }
        }
    }
}
