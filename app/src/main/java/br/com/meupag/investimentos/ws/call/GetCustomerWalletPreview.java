package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Date;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.FmtUtils;
import br.com.meupag.investimentos.ws.transferobject.WalletPreviewTO;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class GetCustomerWalletPreview extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = GetCustomerWalletPreview.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_CUSTOMER_WALLET_PREVIEW;
    private static final Class<WalletPreviewTO> clazz = WalletPreviewTO.class;

    private ResponseHandler responseHandler;

    RetornoWS<WalletPreviewTO> retornoWS;
    IOException ioException;

    public final RequestParams requestParams = new RequestParams();

    public static class RequestParams{
        final private String authKey = Constantes.AUTH_KEY;
        final private String referenceDate = FmtUtils.YYYY_MM_DD.format(new Date());
        public String customerId;
    }

    public interface ResponseHandler {
        void onSuccess(RetornoWS<WalletPreviewTO> retornoWS);
        void onUnsuccess(RetornoWS<WalletPreviewTO> retornoWS);
        void onError(IOException ioException);
    }

    public GetCustomerWalletPreview setResponseHandler(ResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
        return this;
    }

    private String paramsToQueryString(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(URL).newBuilder();
        urlBuilder.addQueryParameter("customerId", requestParams.customerId);
        urlBuilder.addQueryParameter("authKey", requestParams.authKey);
        urlBuilder.addQueryParameter("referenceDate", requestParams.referenceDate);
        return urlBuilder.build().toString();
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{

            Request request = new Request.Builder()
                    .url(paramsToQueryString())
                    .get()
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            ResponseBody responseBody = OK_HTTP_CLIENT.newCall(request).execute().body();

            if(responseBody != null){
                final String jsonResponse = responseBody.string();
                Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

                ObjectMapper mapper = Constantes.mapper;
                final JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class,
                        WalletPreviewTO.class);

                retornoWS = mapper.readValue(jsonResponse, type);

                return retornoWS.getStatus();
            }


        } catch (UnknownHostException e){
            this.ioException = e;
        } catch (IOException e){
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {

        if(success != null && responseHandler != null){
            if(ioException != null){
                responseHandler.onError(ioException);
                return;
            }

            if(success){
                responseHandler.onSuccess(retornoWS);
            } else {
                Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
                responseHandler.onUnsuccess(retornoWS);
            }
        }


    }
}
