package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.ws.transferobject.ProductDomainTO;
import br.com.meupag.investimentos.util.HttpHelper;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class GetDomain extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = GetAccountRecover.class.getSimpleName();
    private static final Class<ProductDomainTO> clazz = ProductDomainTO.class;

    private static final String URL = Constantes.HOST_AWS + Constantes.WS_PRODUCT_DOMAIN;
    private final ObjectMapper mapper = Constantes.mapper;
    private final OkHttpClient okHttpClient = HttpHelper.OK_HTTP_CLIENT;

    private ResponseHandler responseHandler;

    RetornoWS<ProductDomainTO> retornoWS;
    IOException ioException;


    public interface ResponseHandler {
        void onSuccess(ProductDomainTO data);
        void onUnsuccess(RetornoWS<ProductDomainTO> retornoWS);
        void onError(IOException ioException);
    }

    public GetDomain setResponseHandler(ResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
        return this;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{

            Request request = new Request.Builder()
                    .url(URL)
                    .get()
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = okHttpClient.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if(responseBody != null){
                final JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, clazz);
                final String json = responseBody.string();
                retornoWS = mapper.readValue(json, type);

                System.out.println("RETORNO DO SERVICO: " + json);

                return retornoWS.getStatus();
            }


        }catch (IOException e){
            ioException = e;
        }

        return null;
    }

    @Override
    protected void onPostExecute(Boolean success) {

        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS.getData());
        } else {
            Log.e(TAG, "Não foi possível recuperar AccountTO\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }


    private static GetDomain getDomain;

    public static void doCall(GetDomain.ResponseHandler rh){
        if(getDomain != null){
            getDomain.cancel(true);
        }
        getDomain = new GetDomain();
        getDomain.setResponseHandler(rh);
        getDomain.execute();
    }
}
