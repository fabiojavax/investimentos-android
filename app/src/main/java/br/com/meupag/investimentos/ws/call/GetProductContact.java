package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class GetProductContact extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = GetProductContact.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_PRODUCT_CONTACT;
    private static final Class<Object> clazz = Object.class;

    RetornoWS<Object> retornoWS;
    IOException ioException;

    private ResponseHandler responseHandler;
    private RequestParams requestParams;

    public static class RequestParams{
        public String customerId;
        public String phoneNumber;
    }

    public interface ResponseHandler{
        void onSuccess(Object data);
        void onUnsuccess(RetornoWS<Object> retornoWS);
        void onError(IOException iOException);
    }

    public GetProductContact setRensponseHandler(ResponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    public GetProductContact setRequestParams(RequestParams requestParams) {
        this.requestParams = requestParams;
        return this;
    }

    private String paramsToQueryString(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(URL).newBuilder();
        urlBuilder.addQueryParameter("customerId", requestParams.customerId);
        urlBuilder.addQueryParameter("phoneNumber", requestParams.phoneNumber);
        return urlBuilder.build().toString();
    }

    @Override
    protected void onPreExecute() {
        if(responseHandler == null){
            Log.d(TAG, "Favor implementar o ResponseHandler");
            GetProductContact.this.cancel(true);
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{

            Request request = new Request.Builder()
                    .url(paramsToQueryString())
                    .get()
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = OK_HTTP_CLIENT.newCall(request).execute();

            ResponseBody responseBody = response.body();

            if(responseBody == null){
                return false;
            }

            final String jsonResponse = responseBody.string();
            Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

            ObjectMapper mapper = Constantes.mapper;
            JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, clazz);
            retornoWS = mapper.readValue(jsonResponse, type);

            return retornoWS.getStatus();


        }catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {

        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS.getData());
        } else {
            Log.e(TAG, "Não foi possível recuperar AccountTO\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }


    private static GetProductContact getProductContact;

    public static void doCall(GetProductContact.RequestParams rp,
                              GetProductContact.ResponseHandler rh){
        if(getProductContact != null){
            getProductContact.cancel(true);
        }
        getProductContact = new GetProductContact();
        getProductContact.setRensponseHandler(rh);
        getProductContact.setRequestParams(rp);
        getProductContact.execute();
    }
}
