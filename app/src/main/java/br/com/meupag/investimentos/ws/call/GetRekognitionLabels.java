package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.RetornoListaWS;
import okhttp3.HttpUrl;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class GetRekognitionLabels extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = GetRekognitionLabels.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_REKOGNITION_LABELS;
    private static final Class clazz = String.class;

    RetornoListaWS<String> retornoWS;
    IOException ioException;

    private ResponseHandler responseHandler;
    private RequestParams requestParams;

    public static class RequestParams{
        public String customerId;
        public String objectId;
    }

    public interface ResponseHandler{
        void onSuccess(List<String> data);
        void onUnsuccess(RetornoListaWS<String> retornoWS);
        void onError(IOException iOException);
    }

    public GetRekognitionLabels setRensponseHandler(ResponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    public GetRekognitionLabels setRequestParams(RequestParams requestParams) {
        this.requestParams = requestParams;
        return this;
    }

    private String paramsToQueryString(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(URL).newBuilder();
        urlBuilder.addQueryParameter("customerId", requestParams.customerId);
        urlBuilder.addQueryParameter("objectId", requestParams.objectId);
        return urlBuilder.build().toString();
    }

    @Override
    protected void onPreExecute() {
        if(responseHandler == null){
            Log.d(TAG, "Favor implementar o ResponseHandler");
            GetRekognitionLabels.this.cancel(true);
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        ObjectMapper mapper = Constantes.mapper;

        try{

            Request request = new Request.Builder()
                    .url(paramsToQueryString())
                    .get()
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = OK_HTTP_CLIENT.newCall(request).execute();

            if(response.isSuccessful()){
                ResponseBody responseBody = response.body();

                if(responseBody == null){
                    return false;
                }

                final String jsonResponse = responseBody.string();
                Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoListaWS.class, clazz);
                retornoWS = mapper.readValue(jsonResponse, type);

                return retornoWS.getStatus();

            } else {
                return false;
            }


        }catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS.getData());
        } else {
            Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }
}
