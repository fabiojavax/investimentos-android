package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.ws.transferobject.TaxTO;
import br.com.meupag.investimentos.util.HttpHelper;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class GetStockTax extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = GetStockTax.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_STOCK_TAX;
    private static final Class clazz = TaxTO.class;

    RetornoWS<TaxTO> retornoWS;
    private IOException ioException;

    private ResponseHandler responseHandler;
    private RequestParams requestParams;

    public static class RequestParams{
        public String customerId;
        public String stockId;
        public String buyDate;
        public final String authKey = Constantes.AUTH_KEY;
    }

    public interface ResponseHandler{
        void onSuccess(TaxTO data);
        void onUnsuccess(RetornoWS<TaxTO> retornoWS);
        void onError(IOException iOException);
    }

    public GetStockTax setRensponseHandler(ResponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    public GetStockTax setRequestParams(RequestParams requestParams) {
        this.requestParams = requestParams;
        return this;
    }

    private String paramsToQueryString(){
        HttpUrl.Builder urlBuilder = HttpUrl.parse(URL).newBuilder();
        urlBuilder.addQueryParameter("customerId", requestParams.customerId);
        urlBuilder.addQueryParameter("stockId", requestParams.stockId);
        urlBuilder.addQueryParameter("authKey", requestParams.authKey);
        urlBuilder.addQueryParameter("buyDate", requestParams.buyDate);
        return urlBuilder.build().toString();
    }

    @Override
    protected void onPreExecute() {
        if(responseHandler == null){
            Log.d(TAG, "Favor implementar o ResponseHandler");
            GetStockTax.this.cancel(true);
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        ObjectMapper mapper = Constantes.mapper;

        try{

            Request request = new Request.Builder()
                    .url(paramsToQueryString())
                    .get()
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = OK_HTTP_CLIENT.newCall(request).execute();

            if(response.isSuccessful()){
                ResponseBody responseBody = response.body();

                if(responseBody == null){
                    return false;
                }

                final String jsonResponse = responseBody.string();
                Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, clazz);
                retornoWS = mapper.readValue(jsonResponse, type);

                return retornoWS.getStatus();

            } else {
                return false;
            }


        }catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS.getData());
        } else {
            Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }
}
