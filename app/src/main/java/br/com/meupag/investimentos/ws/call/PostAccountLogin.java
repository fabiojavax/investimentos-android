package br.com.meupag.investimentos.ws.call;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.ws.transferobject.AccountTO;
import br.com.meupag.investimentos.ws.transferobject.LoginTO;
import br.com.meupag.investimentos.util.HttpHelper;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.JSON_MEDIA_TYPE;
import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class PostAccountLogin extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = PostAccountLogin.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_ACCOUNT_LOGIN;


    RetornoWS<LoginTO> retornoWS;
    IOException ioException;

    private ResponseHandler responseHandler;
    private RequestParams requestParams;

    public static class RequestParams{
        public String customerId;
        public String password;
    }

    public interface ResponseHandler{
        void onSuccess(LoginTO data);
        void onUnsuccess(RetornoWS<LoginTO> retornoWS);
        void onError(IOException iOException);
    }

    public PostAccountLogin setRensponseHandler(ResponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    public PostAccountLogin setRequestParams(RequestParams requestParams) {
        this.requestParams = requestParams;
        return this;
    }

    @Override
    protected void onPreExecute() {
        if(responseHandler == null){
            Log.d(TAG, "Favor implementar o ResponseHandler");
            PostAccountLogin.this.cancel(true);
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        ObjectMapper mapper = Constantes.mapper;

        try{

            RequestBody body = RequestBody.create(JSON_MEDIA_TYPE, mapper.writeValueAsString(requestParams));

            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = OK_HTTP_CLIENT.newCall(request).execute();

            ResponseBody responseBody = response.body();

            if(responseBody == null){
                return false;
            }

            final String jsonResponse = responseBody.string();
            Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

            JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, LoginTO.class);
            retornoWS = mapper.readValue(jsonResponse, type);

            return retornoWS.getStatus();


        }catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS.getData());
        } else {
            Log.e(TAG, "Não foi possível recuperar AccountTO\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }


    private static PostAccountLogin postAccountLogin;

    public static void doCall(Activity act, PostAccountLogin.RequestParams rp,
                              PostAccountLogin.ResponseHandler rh){
        if(postAccountLogin != null){
            postAccountLogin.cancel(true);
        }
        postAccountLogin = new PostAccountLogin();
        postAccountLogin.setRensponseHandler(rh);
        postAccountLogin.setRequestParams(rp);
        postAccountLogin.execute();
    }
}
