package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.math.BigDecimal;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.JSON_MEDIA_TYPE;
import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class PostAccountProposal extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = PostAccountProposal.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_ACCOUNT_PROPOSAL;
    private static final Class clazz = Object.class;

    public RequestParams requestParams;

    private RensponseHandler responseHandler;

    RetornoWS<Object> retornoWS;
    private IOException ioException;


    public interface RensponseHandler {
        void onSuccess(RetornoWS<Object> retornoWS);
        void onUnsuccess(RetornoWS<Object> retornoWS);
        void onError(IOException iOException);
    }

    public PostAccountProposal setRensponseHandler(RensponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    public PostAccountProposal(RequestParams requestParams) {
        this.requestParams = requestParams;
    }

    public static class RequestParams{
        public String customerId;//": "10424056704",
        public String name;//": "FABIO LEANDRO BELLO LEMOS",
        public String motherName;//": "MERE MARA BELLO LEMOS",
        public String gender;//": "M",
        public String country;//": "BR",
        public String civilStatus;//": "C",
        public String birthDate;//": "1983-06-16",
        public String birthCityCode;//": "5305",
        public String birthCityDescription;//": "VITORIA",
        public String birthState;//": "ES",
        public String documentType;//": "1",
        public String documentNumber;//": "1793514",
        public String documentIssuer;//": "01",
        public String documentState;//": "ES",
        public String documentEmission;//": "2018-02-28",
        public String documentExpiration;//": "2023-02-26",
        public String occupation;//": "1",
        public String email;//": "fabiojavax@gmail.com",
        public String zipCode;//": "29090640",
        public String city;//": "VITORIA",
        public String neighborhood;//": "JARDIM CAMBURI",
        public String streetAddress;//": "AV HERWAN MODENESE WANDERLEY",
        public String complement;//": "CASA",
        public String streetNumber;//": "224",
        public String state;//": "ES",
        public String phone;//": "27997174354",
        public String ppe;//": "N",
        public String bankCode;//": "0001",
        public String bankAgency;//": "2921",
        public String accountNumber;//": "1072757",
        public String accountType;//": "1",
        public Integer status;//": 1
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{
            final ObjectMapper mapper = Constantes.mapper;

            String json = mapper.writeValueAsString(requestParams);

            Log.d(TAG, "Enviando JSON: " + mapper.writeValueAsString(requestParams));

            RequestBody body = RequestBody.create(JSON_MEDIA_TYPE, json);


            Log.d(TAG, "Enviando proposta " + body.toString());
            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            ResponseBody responseBody = OK_HTTP_CLIENT.newCall(request).execute().body();

            if(responseBody != null){
                String responseJson = responseBody.string();
                Log.d(TAG, Constantes.SUCCESSFUL_POST + responseJson);

                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, clazz);
                retornoWS = mapper.readValue(responseJson, type);

                return retornoWS.getStatus();
            }

        } catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS);
        } else {
            Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }

}
