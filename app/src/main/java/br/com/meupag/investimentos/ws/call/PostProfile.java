package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.transferobject.ProfileTO;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.JSON_MEDIA_TYPE;
import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class PostProfile extends AsyncTask<ProfileTO, String, Boolean> {

    private static final String TAG = PostProfile.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_CUSTOMER_PROFILE;

    private RensponseHandler rensponseHandler;
    private RetornoWS<Object> retornoWS;

    public interface RensponseHandler {
        void onSuccess();
        void onError(RetornoWS<Object> retornoWS);
    }

    public PostProfile setRensponseHandler(RensponseHandler rensponseHandler) {
        this.rensponseHandler = rensponseHandler;
        return this;
    }

    @Override
    protected Boolean doInBackground(ProfileTO... profiles) {
        ProfileTO profileTO = profiles[0];
        if(profileTO == null) return false;

        try{
            ObjectMapper mapper = Constantes.mapper;
            String json = mapper.writeValueAsString(profileTO);

            Request request = new Request.Builder()
                    .url(URL)
                    .post(RequestBody.create(JSON_MEDIA_TYPE, json))
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = OK_HTTP_CLIENT.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if(responseBody != null){
                String responseJson = responseBody.string();
                Log.d(TAG, "RETORNO DO SERVICO: " + responseJson);

                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, Object.class);
                retornoWS = mapper.readValue(responseJson, type);

                return retornoWS.getStatus();
            }

        } catch (JsonProcessingException e){
            Log.d(TAG, e.getMessage());
        } catch (IOException e){
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(rensponseHandler != null && success){
            rensponseHandler.onSuccess();
        }else if(success == null || !success){
            Log.e(TAG, "Não foi possível alterar o menu_profile:\n"+retornoWS.getMessage());
            rensponseHandler.onError(retornoWS);
        }
    }
}
