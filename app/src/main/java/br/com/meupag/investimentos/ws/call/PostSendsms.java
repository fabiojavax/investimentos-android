package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.transferobject.TokenTO;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.JSON_MEDIA_TYPE;
import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class PostSendsms extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = PostSendsms.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_SENDSMS;
    private static final Class clazz = TokenTO.class;

    public RequestParams requestParams = new RequestParams();

    private ResponseHandler responseHandler;

    RetornoWS<TokenTO> retornoWS;
    private IOException ioException;

    public static class RequestParams{
        public String customerId;
    }

    public interface ResponseHandler {
        void onSuccess(RetornoWS<TokenTO> retornoWS);
        void onUnsuccess(RetornoWS<TokenTO> retornoWS);
        void onError(IOException iOException);
    }

    public PostSendsms setResponseHandler(ResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
        return this;
    }

    public PostSendsms setRequestParams(RequestParams requestParams) {
        this.requestParams = requestParams;
        return this;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{
            final ObjectMapper mapper = Constantes.mapper;

            RequestBody body = RequestBody.create(JSON_MEDIA_TYPE, mapper.writeValueAsString(requestParams));


            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = OK_HTTP_CLIENT.newCall(request).execute();

            ResponseBody responseBody = response.body();

            if(responseBody == null){
                return false;
            }

            final String jsonResponse = responseBody.string();
            Log.d(TAG, "RETORNO DO SERVICO: " + jsonResponse);

            JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, clazz);
            retornoWS = mapper.readValue(jsonResponse, type);

            return retornoWS.getStatus();


        }catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS);
        } else {
            Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }

}
