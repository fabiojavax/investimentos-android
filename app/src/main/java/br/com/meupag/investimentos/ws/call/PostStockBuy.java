package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.math.BigDecimal;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.transferobject.LoginTO;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.internal.http.RealResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.JSON_MEDIA_TYPE;
import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class PostStockBuy extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = PostStockBuy.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_STOCK_BUY;
    private static final Class clazz = Object.class;

    public final RequestParams requestParams = new RequestParams();

    private RensponseHandler responseHandler;

    RetornoWS<Object> retornoWS;
    private IOException ioException;


    public interface RensponseHandler {
        void onSuccess(RetornoWS<Object> retornoWS);
        void onUnsuccess(RetornoWS<Object> retornoWS);
        void onError(IOException iOException);
    }

    public PostStockBuy setRensponseHandler(RensponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    public static class RequestParams{
        public String customerId;
        public String accountNumber;
        public BigDecimal amount;
        public Integer period;
        public Integer stockType;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{
            final ObjectMapper mapper = Constantes.mapper;

            RequestBody requestBody = new FormBody.Builder()
                    .add("accountNumber", String.valueOf(requestParams.accountNumber))
                    .add("period", String.valueOf(requestParams.period))
                    .add("amount", String.valueOf(requestParams.amount))
                    .add("stockType", String.valueOf(requestParams.stockType))
                    .add("customerId", requestParams.customerId)
                    .add("authKey", Constantes.AUTH_KEY)
                    .build();


            Log.d(TAG, "Enviando Solicitacão de compra " + requestBody.toString());
            Request request = new Request.Builder()
                    .url(URL)
                    .post(requestBody)
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            ResponseBody responseBody = OK_HTTP_CLIENT.newCall(request).execute().body();

            if(responseBody != null){
                String responseJson = responseBody.string();
                Log.d(TAG, Constantes.SUCCESSFUL_POST + responseJson);

                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, clazz);
                retornoWS = mapper.readValue(responseJson, type);

                return retornoWS.getStatus();
            }

        } catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS);
        } else {
            Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }

}
