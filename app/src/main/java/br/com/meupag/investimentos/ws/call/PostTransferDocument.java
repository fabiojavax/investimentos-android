package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;
import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.RetornoWS;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import java.io.IOException;
import java.math.BigDecimal;

import static br.com.meupag.investimentos.util.HttpHelper.OK_HTTP_CLIENT;

public class PostTransferDocument extends AsyncTask<Void, String, Boolean> {

    private static final String URL = Constantes.HOST_AWS + Constantes.WS_TRANSFER_DOCUMENT;

    String filename;

    public PostTransferDocument(String filename) {
        this.filename = filename;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{

            RequestBody requestBody = new FormBody.Builder()
                    .add("filename", filename)
                    .add("authKey", Constantes.AUTH_KEY)
                    .build();

            Request request = new Request.Builder()
                    .url(URL)
                    .post(requestBody)
                    .addHeader("Authorization", Constantes.TOKEN)
                    .build();

            OK_HTTP_CLIENT.newCall(request).execute();

            return true;

        } catch (IOException e){
            e.printStackTrace();
        }

        return false;

    }

}
