package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.HttpHelper;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.transferobject.LoginTO;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.JSON_MEDIA_TYPE;

public class PostWithdraw extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = PostWithdraw.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_CUSTOMER_WITHDRAW;
    private static final Class clazz = Object.class;

    public final RequestParams requestParams = new RequestParams();

    private RensponseHandler responseHandler;

    RetornoWS<Object> retornoWS;
    private IOException ioException;

    public interface RensponseHandler {
        void onSuccess(RetornoWS<Object> retornoWS);
        void onUnsuccess(RetornoWS<Object> retornoWS);
        void onError(IOException iOException);
    }

    public PostWithdraw setRensponseHandler(RensponseHandler rensponseHandler) {
        this.responseHandler = rensponseHandler;
        return this;
    }

    public static class RequestParams{
        public String customerId;
        public Integer accountNumber;
        public Double amount;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{
            final ObjectMapper mapper = Constantes.mapper;

            RequestBody body = RequestBody.create(JSON_MEDIA_TYPE, mapper.writeValueAsString(requestParams));


            Request request = new Request.Builder()
                    .url(URL)
                    .post(body)
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            ResponseBody responseBody = HttpHelper.OK_HTTP_CLIENT.newCall(request).execute().body();

            if(responseBody != null){
                String responseJson = responseBody.string();
                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, clazz);
                retornoWS = mapper.readValue(responseJson, type);

                Log.d(TAG, Constantes.SUCCESSFUL_POST + responseJson);
                return retornoWS.getStatus();
            }

        } catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS);
        } else {
            Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }

}
