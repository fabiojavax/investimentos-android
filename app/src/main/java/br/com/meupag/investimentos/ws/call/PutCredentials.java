package br.com.meupag.investimentos.ws.call;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.HttpHelper;
import br.com.meupag.investimentos.util.RetornoWS;
import br.com.meupag.investimentos.ws.transferobject.LoginTO;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.JSON_MEDIA_TYPE;

public class PutCredentials extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = PutCredentials.class.getSimpleName();

    public static final String CUSTOMER_ID = "customerId";
    public static final String PASSWORD = "password";
    public static final String NEW_PASSWORD = "newPassword";
    public static final String SIGNATURE = "signature";
    public static final String NEW_SIGNATURE = "newSignature";

    private static final String URL = Constantes.HOST_AWS + Constantes.WS_ACCOUNT_CREDENTIALS;
    private final ObjectMapper mapper = Constantes.mapper;
    private final OkHttpClient okHttpClient = HttpHelper.OK_HTTP_CLIENT;

    private RensponseHandler rensponseHandler;
    private Map<String, String> fields = new HashMap<>();
    private JavaType type;

    RetornoWS<LoginTO> retornoWS;

    public interface RensponseHandler {
        void onSuccess();
        void onError(RetornoWS<LoginTO> retornoWS);
    }

    public  PutCredentials() {
        type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, LoginTO.class);
    }

    public PutCredentials setRensponseHandler(RensponseHandler rensponseHandler) {
        this.rensponseHandler = rensponseHandler;
        return this;
    }

    public PutCredentials addField(String key, String value){
        fields.put(key, value);
        return this;
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{

            JSONObject jsonObject = new JSONObject(fields);
            String json = jsonObject.toString();
            RequestBody body = RequestBody.create(JSON_MEDIA_TYPE, json);

            Request request = new Request.Builder()
                    .url(URL)
                    .put(body)
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            Response response = okHttpClient.newCall(request).execute();
            ResponseBody responseBody = response.body();

            if(responseBody != null){
                String responseJson = responseBody.string();
                retornoWS = mapper.readValue(responseJson, type);

                System.out.println("RETORNO DO SERVICO: " + responseJson);

                return retornoWS.getStatus();
            }

        } catch (IOException e){
            e.printStackTrace();
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(rensponseHandler != null && success){
            rensponseHandler.onSuccess();
            Log.e(TAG, "SUCESSO NA ALTERACAO DE SENHA:\n"+retornoWS.getMessage());
        }else if(success == null || !success){
            Log.e(TAG, "Não foi possível alterar a senha:\n"+retornoWS.getMessage());
            rensponseHandler.onError(retornoWS);
        }
    }

}
