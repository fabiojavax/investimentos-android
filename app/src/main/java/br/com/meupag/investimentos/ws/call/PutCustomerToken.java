package br.com.meupag.investimentos.ws.call;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

import br.com.meupag.investimentos.Constantes;
import br.com.meupag.investimentos.util.HttpHelper;
import br.com.meupag.investimentos.util.RetornoWS;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

import static br.com.meupag.investimentos.util.HttpHelper.JSON_MEDIA_TYPE;

public class PutCustomerToken extends AsyncTask<Void, String, Boolean> {

    private static final String TAG = PutCustomerToken.class.getSimpleName();
    private static final String URL = Constantes.HOST_AWS + Constantes.WS_CUSTOMER_TOKEN;
    private static final Class clazz = Object.class;

    public RequestParams requestParams;

    private ResponseHandler responseHandler;

    RetornoWS<Object> retornoWS;
    private IOException ioException;

    public interface ResponseHandler {
        void onSuccess(RetornoWS<Object> retornoWS);
        void onUnsuccess(RetornoWS<Object> retornoWS);
        void onError(IOException iOException);
    }

    public PutCustomerToken setRensponseHandler(ResponseHandler responseHandler) {
        this.responseHandler = responseHandler;
        return this;
    }

    public static class RequestParams{
        public String customerId;
        public String deviceToken;
        public final int platform = 1;
    }

    public PutCustomerToken setRequestParams(RequestParams requestParams) {
        this.requestParams = requestParams;
        return this;
    }

    @Override
    protected void onPreExecute() {
        if(responseHandler == null){
            Log.d(TAG, "Favor implementar o ResponseHandler");
            PutCustomerToken.this.cancel(true);
        }
    }

    @Override
    protected Boolean doInBackground(Void... voids) {

        try{
            final ObjectMapper mapper = Constantes.mapper;

            RequestBody body = RequestBody.create(JSON_MEDIA_TYPE, mapper.writeValueAsString(requestParams));


            Request request = new Request.Builder()
                    .url(URL)
                    .put(body)
                    .addHeader("Authorization", Constantes.TOKEN)//todo acertar segurança do token
                    .build();

            ResponseBody responseBody = HttpHelper.OK_HTTP_CLIENT.newCall(request).execute().body();

            if(responseBody != null){
                String responseJson = responseBody.string();
                JavaType type = mapper.getTypeFactory().constructParametricType(RetornoWS.class, clazz);
                retornoWS = mapper.readValue(responseJson, type);

                Log.d(TAG, Constantes.SUCCESSFUL_POST + responseJson);
                return retornoWS.getStatus();
            }

        } catch (IOException e){
            ioException = e;
        }

        return false;
    }

    @Override
    protected void onPostExecute(Boolean success) {
        if(ioException != null){
            responseHandler.onError(ioException);
            return;
        }

        if(success){
            responseHandler.onSuccess(retornoWS);
        } else {
            Log.e(TAG, "Não foi possível recuperar " + clazz.getSimpleName() + "\n" + retornoWS.getMessage());
            responseHandler.onUnsuccess(retornoWS);
        }
    }

    private static PutCustomerToken putCustomerToken;

    public static void doCall(Activity act, PutCustomerToken.RequestParams rp,
                              PutCustomerToken.ResponseHandler rh){
        if(putCustomerToken != null){
            putCustomerToken.cancel(true);
        }
        putCustomerToken = new PutCustomerToken();
        putCustomerToken.setRensponseHandler(rh);
        putCustomerToken.setRequestParams(rp);
        putCustomerToken.execute();
    }

}
