package br.com.meupag.investimentos.ws.transferobject;

public class AddressTO {
    public String cep;
    public String logradouro;
    public String complemento;
    public String bairro;
    public String localidade;
    public String uf;
}
