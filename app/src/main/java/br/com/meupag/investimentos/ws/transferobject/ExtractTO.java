package br.com.meupag.investimentos.ws.transferobject;

import java.util.List;

public class ExtractTO {
    public String mensagem;
    public String nomeCliente;
    public Movimentacoes movimentacoes;

    public static class Movimentacoes{
        public List<MovimentacaoTO> movimentacaoCarteira;
    }

    public static class MovimentacaoTO{
        public String dataOperacao;//": "2018-11-22",
        public String tipoOperacao;//": "RESGATE TOTAL",
        public String numTitulo;//": 72783,
        public Double valorPrincipal;//": 20,
        public Double valorRendimento;//": 0.18,
        public Double valorBruto;//": 20.18,
        public Double remuneracao;//": 132,
        public Double valorImpostos;//": 0,
        public Double valorIR;//": 0,
        public Double valorIOF;//": 0,
        public Double valorLiquido;//": 20.14,
        public String descricaoProduto;//": "Rendimento Azul"
    }

}
