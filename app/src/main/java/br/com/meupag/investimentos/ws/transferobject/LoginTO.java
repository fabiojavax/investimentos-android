package br.com.meupag.investimentos.ws.transferobject;

public class LoginTO {

    public String customerId;
    public String password;
    public String newPassword;
    public String signature;
    public String newSignature;
    public String accountNumber;

}
