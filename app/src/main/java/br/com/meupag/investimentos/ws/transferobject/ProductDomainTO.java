package br.com.meupag.investimentos.ws.transferobject;

import java.util.List;

public class ProductDomainTO {
    public List<DomainTO> occupations;
    public List<DomainTO> documentTypes;
    public List<DomainTO> documentIssuers;
    public List<DomainTO> civilStates;
    public List<DomainTO> states;
    public List<DomainTO> banks;
    public List<DomainTO> accountTypes;

    public static class DomainTO{
        public String code;
        public String description;
    }
}
