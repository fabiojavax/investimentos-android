package br.com.meupag.investimentos.ws.transferobject;

public class ProfileTO {
    public String customerId;
    public String phone;
    public String email;
    public String name;
    public BankInfoTO bankInfo = new BankInfoTO();
    public String signature;

    public static class BankInfoTO {
        public String bankCode;
        public String bankAgency;
        public String accountType;
        public String accountNumber;
    }
}
