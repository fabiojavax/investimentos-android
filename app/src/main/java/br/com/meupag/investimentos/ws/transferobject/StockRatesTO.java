package br.com.meupag.investimentos.ws.transferobject;

import java.util.ArrayList;
import java.util.List;

public class StockRatesTO {

    //    [15:38, 1/11/2018] Fabio Lemos: 0 = POS FIXADO
    //    [15:38, 1/11/2018] Fabio Lemos: 1 = PRE FIXADO
    public static final int POS_FIXADO = 0;
    public static final int PRE_FIXADO = 1;

    public List<RateTO> rates = new ArrayList<>();

    public static class RateTO {
        public Integer period;
        public Integer days;
        public Float percentageCDI;
        public Float rate;
        public String expirationDate;
        public Integer workingDays;
        public Float savingsRate;
        public Float savingsPercentage;
    }
}
