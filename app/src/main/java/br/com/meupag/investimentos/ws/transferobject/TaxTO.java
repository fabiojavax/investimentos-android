package br.com.meupag.investimentos.ws.transferobject;

public class TaxTO {
    public static final Integer IOF = 1;
    public static final Integer IRRF = 2;

    public Integer type;
    public Double currentDiscount;
    public Integer remainingDays;
}
