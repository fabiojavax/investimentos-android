package br.com.meupag.investimentos.ws.transferobject;

import java.util.ArrayList;
import java.util.List;

public class WalletPreviewTO {

    public String customerId;//": "10424056704",
    public String referenceDate;//": "2018-10-04",
    public Double totalInvestedAmount;//": 230,
    public Double totalCurrentAmount;//": 230.13,
    public Double totalRevenue;//": 0.13,
    public Double totalTax;//": 0,
    public List<ProductTO> products = new ArrayList<>();//": [

    public static class ProductTO {
        public Long productId;//":23,
        public String name;//":"RDB AVISTA POS",
        public Double investedAmount;//":80,
        public Double currentAmount;//":80.13,
        public Double revenue;//":0.13,
        public Double tax;//":0.13,
    }

}