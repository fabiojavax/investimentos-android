package br.com.meupag.investimentos.ws.transferobject;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class WalletTO{
    public TitulosTO titulos;

    public static class TitulosTO {
        public List<TituloTO> titulo = new ArrayList<>();
    }

    public static class TituloTO implements Parcelable{

        public static final String NAME = "TituloTO";
        public Integer codigoPapel;//": 24,
        public String descricaoPapel;//": "RDB AZUL PRE FGC",
        public Integer numeroTitulo;//": 71833,
        public String dataEmissao;//": "2018-10-04",
        public String dataVencimento;//": "2024-09-02",
        public Integer prazo;//": 2160,
        public Double taxa;//": 14.56,
        public Double valorPrincipal;//": 50,
        public Double valorRendimento;//": 0.37,
        public Double valorAtualizado;//": 50.37,
        public Double valorResgateBruto;//": 50.37,
        public Double valorImposto;//": 0,
        public Double valorIR;//": 0.05,
        public Double valorIOF;//": 0.11,
        public Double valorResgateLiquido;//": 50.21,
        public Double rendimentoSobrePoupanca;//": 85,
        public Double projecaoVencimento;//": 65,
        public Integer indexadorResgate;//": 1,
        public Integer stockType;//": 1

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(codigoPapel);
            dest.writeString(descricaoPapel);
            dest.writeInt(numeroTitulo);
            dest.writeString(dataEmissao);
            dest.writeString(dataVencimento);
            dest.writeInt(prazo);
            dest.writeDouble(taxa);
            dest.writeDouble(valorPrincipal);
            dest.writeDouble(valorRendimento);
            dest.writeDouble(valorAtualizado);
            dest.writeDouble(valorResgateBruto);
            dest.writeDouble(valorImposto);
            dest.writeDouble(valorIR);
            dest.writeDouble(valorIOF);
            dest.writeDouble(valorResgateLiquido);
            dest.writeDouble(rendimentoSobrePoupanca);
            dest.writeDouble(projecaoVencimento);
            dest.writeInt(indexadorResgate);
            dest.writeInt(stockType);
        }

        public TituloTO(Parcel parcel) {
            codigoPapel = parcel.readInt();
            descricaoPapel = parcel.readString();
            numeroTitulo = parcel.readInt();
            dataEmissao = parcel.readString();
            dataVencimento = parcel.readString();
            prazo = parcel.readInt();
            taxa = parcel.readDouble();
            valorPrincipal = parcel.readDouble();
            valorRendimento = parcel.readDouble();
            valorAtualizado = parcel.readDouble();
            valorResgateBruto = parcel.readDouble();
            valorImposto = parcel.readDouble();
            valorIR = parcel.readDouble();
            valorIOF = parcel.readDouble();
            valorResgateLiquido = parcel.readDouble();
            rendimentoSobrePoupanca = parcel.readDouble();
            projecaoVencimento = parcel.readDouble();
            indexadorResgate = parcel.readInt();
            stockType = parcel.readInt();

        }

        public TituloTO() {
        }

        public static final Parcelable.Creator<TituloTO> CREATOR = new Parcelable.Creator<TituloTO>() {

            @Override
            public TituloTO createFromParcel(Parcel parcel) {
                return new TituloTO(parcel);
            }

            @Override
            public TituloTO[] newArray(int size) {
                return new TituloTO[0];
            }
        };

    }
}
